package clases;

import java.time.LocalDate;
import java.util.Arrays;

public class Skill {
    private static int idGlobal;
    private int id;
    private String name;
    private String description;
    private int energyUse;
    private int ammoUse;
    private int damage;
    private int healing;
    private float alcance;
    private int[] stats;
    private LocalDate creationDate;

    /**
     * Constructor vacio de la clase Skill. Se usa para la construccion de objetos durante la carga del XML.
     * Solo establece el id del objeto.
     */
    public Skill (){
        idGlobal++;
        this.id = idGlobal;
    }

    /**
     * Constructor de la clase Skill con sus componentes.
     * @param name {@link String}
     * @param description {@link String}
     * @param energyUse {@link Integer}
     * @param ammoUse {@link Integer}
     * @param damage {@link Integer}
     * @param healing {@link Integer}
     * @param alcance {@link Float}
     * @param force {@link Integer}
     * @param agility {@link Integer}
     * @param dextery {@link Integer}
     * @param perception {@link Integer}
     * @param inteligence {@link Integer}
     */
    public Skill(String name, String description, int energyUse, int ammoUse, int damage, int healing, float alcance, int force, int agility, int dextery, int perception, int inteligence) {
        idGlobal++;
        this.id = idGlobal;
        this.name = name;
        this.description = description;
        this.energyUse = energyUse;
        this.ammoUse = ammoUse;
        this.damage = damage;
        this.healing = healing;
        this.alcance = alcance;
        stats = new int[]{force,agility,dextery,perception,inteligence};
        creationDate = LocalDate.now();
    }

    /**
     * Devuelve el atributo Alcance
     * @return java.lang.Floatt
     */
    public float getAlcance() {
        return alcance;
    }

    /**
     * Establece el atributo Alcance
     * @param alcance {@link Float}
     */
    public void setAlcance(float alcance) {
        this.alcance = alcance;
    }

    /**
     * Devuelve el atributo Name
     * @return java.lang.Stringing
     */
    public String getName() {
        return name;
    }

    /**
     * Establece el atributo Name
     * @param name {@link String}
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Devuelve el atributo Biography
     * @return java.lang.Stringg
     */
    public String getDescription() {
        return description;
    }

    /**
     * Establece el atributo Biography
     * @param description {@link String}
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     *  Devulve el atributo Stats
     * @return java.util.Arraysays {@link Integer}nteger
     */
    public int[] getStats() {
        return stats;
    }

    /**
     * Establece el atributo Stats
     * @param stats {@link Arrays} {@link Integer}
     */
    public void setStats(int[] stats) {
        this.stats = stats;
    }

    /**
     * Devuelve el atributo CreationDate
     * @return java.time.LocalDatete
     */
    public LocalDate getCreationDate() {
        return creationDate;
    }

    /**
     * Establece el atributo CreationDate
     * @param creationDate {@link LocalDate}
     */
    public void setCreationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
    }

    public int getEnergyUse() {
        return energyUse;
    }

    public void setEnergyUse(int energyUse) {
        this.energyUse = energyUse;
    }

    public int getAmmoUse() {
        return ammoUse;
    }

    public void setAmmoUse(int ammoUse) {
        this.ammoUse = ammoUse;
    }

    public int getDamage() {
        return damage;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }

    public int getHealing() {
        return healing;
    }

    public void setHealing(int healing) {
        this.healing = healing;
    }


    /**
     * Metodo toString base donde solo se le pasan 5 parametros. Se usara para listar de forma reducida y reconocer el
     * objeto en los Jlist
     * @return java.lang.Stringing
     */
    @Override
    public String toString() {
        return "ID: " + id +
                "- Nombre: " + name + "" +
                ", Alcance: " + alcance +
                ", Descripcion: " + description +
                ", Fecha de Creación: " + creationDate;
    }

    /**
     * Metodo toString que devuelve todos los elementos existentes del objeto. Se usa para mostrar el objeto y todos
     * sus parametros en un jTextArea.
     * @return java.lang.Stringng
     */
    public String toStringComplete() {
                return  "ID: " + id +
                "- Nombre: " + name + "-> \n" +
                "Alcance: " + alcance + "\n" +
                "Descripcion: " + description +
                "\n Stats:" +  "\n" +
                "Fuerza:" + stats[0] +
                ", Agilidad:" + stats[1] +
                ", Destreza:" + stats[2] +
                ", Percepcion:" + stats[3] +
                ", Inteligencia:" + stats[4] +
                "\nEnergia usada: " + energyUse +
                ", Municion usada: " + ammoUse +
                ", Daño producido: " + damage +
                ", Curación producida: " + healing +
                "\nFecha de Creación: " + creationDate;
    }

    /**
     * Metodo usado durante la construccion del XML para obtener cada uno de los elementos del array de forma individual.
     * @param index {@link Integer}
     * @return java.lang.Integerger
     */
    public int getStatsForIndex (int index){
        return stats[index];
    }

    public String toStringSQL(){
        return "INSERT INTO skills " + "(" + " nombre , alcance , descripcion , stats , " +
                "energia usada , municion usada , daño producido , curacion producida , fecha" + ") " + " VALUES " + "("
                + name + "," + alcance + "," + description + "," + "{" + stats[0] + "," + stats[1] + "," + stats[2] + "," + stats[3] + "," + stats[4] + "}" + "," + energyUse + "," + ammoUse
                + "," + damage + "," + healing + "," + creationDate.toString() +");";
    }
}
