package clases;

import java.time.LocalDate;
import java.util.Arrays;

public class Character {
    private static int idGlobal;
    private int id;
    private int age;
    private float alcance;
    private String name;
    private String biography;
    private int[] stats;
    private LocalDate creationDate;

    /**
     * Constructor vacio de la clase Character. Se usa para la construccion de objetos durante la carga del XML.
     * Solo establece el id del objeto.
     */
    public Character (){
        idGlobal++;
        this.id = idGlobal;
    }

    /**
     * Constructor de la clase Character con sus componentes.
     * @param name {@link String}
     * @param age {@link Integer}
     * @param alcance {@link Float}
     * @param biography {@link String}
     * @param force {@link Integer}
     * @param agility {@link Integer}
     * @param dextery {@link Integer}
     * @param perception {@link Integer}
     * @param inteligence {@link Integer}
     */
    public Character(String name, int age, float alcance, String biography, int force, int agility, int dextery, int perception, int inteligence) {
        idGlobal++;
        this.id = idGlobal;
        this.age = age;
        this.alcance = alcance;
        this.name = name;
        this.biography = biography;
        stats = new int[]{force,agility,dextery,perception,inteligence};
        creationDate = LocalDate.now();
    }

    /**
     * Devuelve el atributo Age
     * @return Integer
     */
    public int getAge() {
        return age;
    }

    /**
     * Establece el atributo Age
     * @param age {@link Integer}
     */
    public void setAge(int age) {
        this.age = age;
    }

    /**
     * Devuelve el atributo Alcance
     * @return Float
     */
    public float getAlcance() {
        return alcance;
    }

    /**
     * Establece el atributo Alcance
     * @param alcance {@link Float}
     */
    public void setAlcance(float alcance) {
        this.alcance = alcance;
    }

    /**
     * Devuelve el atributo Name
     * @return String
     */
    public String getName() {
        return name;
    }

    /**
     * Establece el atributo Name
     * @param name {@link String}
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Devuelve el atributo Biography
     * @return String
     */
    public String getBiography() {
        return biography;
    }

    /**
     * Establece el atributo Biography
     * @param biography {@link String}
     */
    public void setBiography(String biography) {
        this.biography = biography;
    }

    /**
     *  Devulve el atributo Stats
     * @return Arrays Integer
     */
    public int[] getStats() {
        return stats;
    }

    /**
     * Establece el atributo Stats
     * @param stats {@link Arrays} {@link Integer}
     */
    public void setStats(int[] stats) {
        this.stats = stats;
    }

    /**
     * Devuelve el atributo CreationDate
     * @return LocalDate
     */
    public LocalDate getCreationDate() {
        return creationDate;
    }

    /**
     * Establece el atributo CreationDate
     * @param creationDate {@link LocalDate}
     */
    public void setCreationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    /**
     * Metodo toString base donde solo se le pasan 5 parametros. Se usara para listar de forma reducida y reconocer el
     * objeto en los Jlist
     * @return - Devuelve una cadena String.
     */
    @Override
    public String toString() {
        return  "ID: " + id +
                "- Nombre: " + name + "" +
                ", Alcance: " + alcance +
                ", Biografia: " + biography +
                ", Fecha de Creación: " + creationDate;
    }

    /**
     * Metodo toString que devuelve todos los elementos existentes del objeto. Se usa para mostrar el objeto y todos
     * sus parametros en un jTextArea.
     * @return java.lang.String
     */
    public String toStringComplete() {
        return  "ID: " + id +
                "- Nombre: " + name + "-> \n" +
                ", Edad: " + age +
                ", Alcance: " + alcance + "\n" +
                "Biografia: " + biography +
                "\n Stats:" +  "\n" +
                "Fuerza:" + stats[0] +
                ", Agilidad:" + stats[1] +
                ", Destreza:" + stats[2] +
                ", Percepcion:" + stats[3] +
                ", Inteligencia:" + stats[4] +
                "\nFecha de Creación: " + creationDate;
    }

    /**
     * Metodo usado durante la construccion del XML para obtener cada uno de los elementos del array de forma individual.
     * @param index {@link Integer}
     * @return Integer
     */
    public int getStatsForIndex (int index){
        return stats[index];
    }

    public String toStringSQL(){
        return "INSERT INTO characters " + "(" + " nombre , edad , alcance , biografia , stats ," +
                " fecha" + ") " + " VALUES " + "("
                + name + "," + age + "," + alcance + "," + biography + "," + "{" + stats[0] + "," + stats[1] + "," + stats[2] + "," + stats[3] + "," + stats[4] + "}" + "," + creationDate.toString() +");";
    }
}
