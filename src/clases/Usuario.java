package clases;

public class Usuario {
    private String baseDeDatosUser;
    private String ip;
    private String puerto;
    private String usuario;
    private String contrasena;
    private int tipoUsuario;

    public Usuario(String baseDeDatosUser, String ip, String puerto, String usuario, String contrasena, int tipoUsuario) {
        this.baseDeDatosUser = baseDeDatosUser;
        this.ip = ip;
        this.puerto = puerto;
        this.usuario = usuario;
        this.contrasena = contrasena;
        this.tipoUsuario = tipoUsuario;
    }

    public String getBaseDeDatosUser() {
        return baseDeDatosUser;
    }

    public void setBaseDeDatosUser(String baseDeDatosUser) {
        this.baseDeDatosUser = baseDeDatosUser;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getPuerto() {
        return puerto;
    }

    public void setPuerto(String puerto) {
        this.puerto = puerto;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public int getTipoUsuario() {
        return tipoUsuario;
    }

    public void setTipoUsuario(int tipoUsuario) {
        this.tipoUsuario = tipoUsuario;
    }

    @Override
    public String toString() {
        return "Usuario{" +
                "baseDeDatosUser='" + baseDeDatosUser + '\'' +
                ", ip='" + ip + '\'' +
                ", puerto='" + puerto + '\'' +
                ", usuario='" + usuario + '\'' +
                ", contrasena='" + contrasena + '\'' +
                ", tipoUsuario=" + tipoUsuario +
                '}';
    }
}
