package clases;

import java.time.LocalDate;
import java.util.Arrays;

public class Object {
    private static int idGlobal;
    private int id;
    private String name;
    private String description;
    private float alcance;
    private int[] stats;
    private LocalDate creationDate;

    /**
     * Constructor vacio de la clase Object. Se usa para la construccion de objetos durante la carga del XML.
     * Solo establece el id del objeto.
     */
    public Object (){
        idGlobal++;
        this.id = idGlobal;
    }

    /**
     * Constructor de la clase Object con sus componentes.
     * @param name {@link String}
     * @param description {@link String}
     * @param alcance {@link Float}
     * @param force {@link Integer}
     * @param agility {@link Integer}
     * @param dextery {@link Integer}
     * @param perception {@link Integer}
     * @param inteligence {@link Integer}
     */
    public Object(String name, String description, float alcance, int force, int agility, int dextery, int perception, int inteligence) {
        idGlobal++;
        this.id = idGlobal;
        this.name = name;
        this.description = description;
        this.alcance = alcance;
        stats = new int[]{force,agility,dextery,perception,inteligence};
        creationDate = LocalDate.now();
    }

    /**
     * Devuelve el atributo Alcance
     * @return Float
     */
    public float getAlcance() {
        return alcance;
    }

    /**
     * Establece el atributo Alcance
     * @param alcance {@link Float}
     */
    public void setAlcance(float alcance) {
        this.alcance = alcance;
    }

    /**
     * Devuelve el atributo Name
     * @return String
     */
    public String getName() {
        return name;
    }

    /**
     * Establece el atributo Name
     * @param name {@link String}
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Devuelve el atributo Description
     * @return String
     */
    public String getDescription() {
        return description;
    }

    /**
     * Establece el atributo Biography
     * @param description {@link String}
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     *  Devulve el atributo Stats
     * @return Arrays Integer
     */
    public int[] getStats() {
        return stats;
    }

    /**
     * Establece el atributo Stats
     * @param stats {@link Arrays} {@link Integer}
     */
    public void setStats(int[] stats) {
        this.stats = stats;
    }

    /**
     * Devuelve el atributo CreationDate
     * @return LocalDate
     */
    public LocalDate getCreationDate() {
        return creationDate;
    }

    /**
     * Establece el atributo CreationDate
     * @param creationDate {@link LocalDate}
     */
    public void setCreationDate(LocalDate creationDate) {
        this.creationDate = creationDate;
    }

    /**
     * Metodo toString base donde solo se le pasan 5 parametros. Se usara para listar de forma reducida y reconocer el
     * objeto en los Jlist
     * @return String
     */
    @Override
    public String toString() {
        return  "ID: " + id +
                "- Nombre: " + name + "" +
                ", Alcance: " + alcance +
                ", Descripcion: " + description +
                ", Fecha de Creación: " + creationDate;
    }

    /**
     * Metodo toString que devuelve todos los elementos existentes del objeto. Se usa para mostrar el objeto y todos
     * sus parametros en un jTextArea.
     * @return String
     */
    public String toStringComplete() {
        return  "ID: " + id +
                "- Nombre: " + name + "-> \n" +
                "Alcance: " + alcance + "\n" +
                "Descripcion: " + description +
                "\n Stats:" +  "\n" +
                "Fuerza:" + stats[0] +
                ", Agilidad:" + stats[1] +
                ", Destreza:" + stats[2] +
                ", Percepcion:" + stats[3] +
                ", Inteligencia:" + stats[4] +
                "\nFecha de Creación: " + creationDate;
    }

    /**
     * Metodo usado durante la construccion del XML para obtener cada uno de los elementos del array de forma individual.
     * @param index
     * @return Integer
     */
    public int getStatsForIndex (int index){
        return stats[index];
    }

    public String toStringSQL(){
        return "INSERT INTO objects " + "(" + " nombre , alcance , descripcion , stats ," +
                " fecha" + ") " + " VALUES " + "("
                + name + "," + alcance + "," + description + "," + "{" + stats[0] + "," + stats[1] + "," + stats[2] + "," + stats[3] + "," + stats[4] + "}" + "," + creationDate.toString() +");";
    }
}
