package modelo;

import clases.Character;
import clases.Object;
import clases.Skill;
import util.Util;

import java.sql.*;
import java.time.LocalDate;

public class BaseDeDatos {

    private String sgbd;
    private String bbdd;
    private String servidor;
    private String puerto;
    private String usuario;
    private String password;
    private int tipo;
    private int error;
    private static String dbMYSQL = "jdbc:mysql://";
    private static String dbPostgreSQL = "jdbc:postgresql://";
    private Connection connection;

    public BaseDeDatos() {
        this.connection = null;
    }

    public BaseDeDatos(String bbdd, String servidor, String puerto, String usuario, String password, int tipo) {
        if (tipo == 0) {
            this.sgbd = dbMYSQL;
        } else if (tipo == 1) {
            this.sgbd = dbPostgreSQL;
        }

        this.bbdd = bbdd;
        this.servidor = servidor;
        this.puerto = puerto;
        this.usuario = usuario;
        this.password = password;
        this.tipo = tipo;

        this.connection = null;
    }

    public void conectar() throws SQLException {
        if (tipo == 0) {
            try {
//                JOptionPane.showMessageDialog(null, sgbd + servidor + ":" + puerto + "/" + bbdd + "----" + usuario + "/" + password);
                Class.forName("com.mysql.jdbc.Driver").newInstance();
                connection = DriverManager.getConnection(sgbd + servidor + ":" + puerto + "/" + bbdd, usuario, password);
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
                error = 0;
            } catch (IllegalAccessException e) {
                e.printStackTrace();
                error = 1;
            } catch (InstantiationException e) {
                e.printStackTrace();
                error = 2;
            }

        } else if (tipo == 1) {
            try {
                Class.forName("org.postgresql.Driver").newInstance();
                connection = DriverManager.getConnection(sgbd + servidor + ":" + puerto + "/" + bbdd, usuario, password);
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InstantiationException e) {
                e.printStackTrace();
            }

        }

    }

    public void desconectar() throws SQLException{
        try {
            if (connection != null) {
                connection.close();
                connection = null;
            }
//            connection.close();
//            connection = null;
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public boolean estadoConexion() {

        if (connection != null)
            return true;

        return false;
    }

    public void upCharacter(Character character) {
        PreparedStatement preparedStatement = null;
        String queryStats = "INSERT INTO stats_all (fuerza, agilidad, destreza, percepcion, inteligencia) VALUES (?, ?, ?, ?, ?)";
        String queryPersonaje = "INSERT INTO personajes (nombre, edad, alcance, biografia, creation_date, stats_all_id) VALUES (?, ?, ?, ?, ?, ?)";
        try {
            connection.setAutoCommit(false);
            preparedStatement = connection.prepareStatement(queryStats, PreparedStatement.RETURN_GENERATED_KEYS);
            preparedStatement.setInt(1, character.getStats()[0]);
            preparedStatement.setInt(2, character.getStats()[1]);
            preparedStatement.setInt(3, character.getStats()[2]);
            preparedStatement.setInt(4, character.getStats()[3]);
            preparedStatement.setInt(5, character.getStats()[4]);
            preparedStatement.executeUpdate();

            ResultSet idGenerados = preparedStatement.getGeneratedKeys();
            idGenerados.next();
            int idStats = idGenerados.getInt(1);
            preparedStatement.close();

            preparedStatement = connection.prepareStatement(queryPersonaje);
            preparedStatement.setString(1, character.getName());
            preparedStatement.setInt(2, character.getAge());
            preparedStatement.setDouble(3, character.getAlcance());
            preparedStatement.setString(4, character.getBiography());
            preparedStatement.setDate(5, Date.valueOf(character.getCreationDate()));
            preparedStatement.setInt(6, idStats);
            preparedStatement.executeUpdate();

            // Valida la transacción
            connection.commit();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (preparedStatement != null)
                try {
                    preparedStatement.close();
                    //resultado.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    public void upObject(Object object) {
        PreparedStatement preparedStatement = null;
        String queryStats = "INSERT INTO stats_all (fuerza, agilidad, destreza, percepcion, inteligencia) VALUES (?, ?, ?, ?, ?)";
        String queryPersonaje = "INSERT INTO objetos (nombre, alcance, descripcion, creation_date, stats_all_id) VALUES (?, ?, ?, ?, ?)";
        try {
            connection.setAutoCommit(false);
            preparedStatement = connection.prepareStatement(queryStats, PreparedStatement.RETURN_GENERATED_KEYS);
            preparedStatement.setInt(1, object.getStats()[0]);
            preparedStatement.setInt(2, object.getStats()[1]);
            preparedStatement.setInt(3, object.getStats()[2]);
            preparedStatement.setInt(4, object.getStats()[3]);
            preparedStatement.setInt(5, object.getStats()[4]);
            preparedStatement.executeUpdate();

            ResultSet idGenerados = preparedStatement.getGeneratedKeys();
            idGenerados.next();
            int idStats = idGenerados.getInt(1);
            preparedStatement.close();

            preparedStatement = connection.prepareStatement(queryPersonaje);
            preparedStatement.setString(1, object.getName());
            preparedStatement.setDouble(2, object.getAlcance());
            preparedStatement.setString(3, object.getDescription());
            preparedStatement.setDate(4, Date.valueOf(object.getCreationDate()));
            preparedStatement.setInt(5, idStats);
            preparedStatement.executeUpdate();

            // Valida la transacción
            connection.commit();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (preparedStatement != null)
                try {
                    preparedStatement.close();
                    //resultado.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    public void upSkill(Skill skill) {
        PreparedStatement preparedStatement = null;
        String queryStats = "INSERT INTO stats_all (fuerza, agilidad, destreza, percepcion, inteligencia) VALUES (?, ?, ?, ?, ?)";
        String queryPersonaje = "INSERT INTO habilidades (nombre, alcance, descripcion, usada_energia, usada_municion, producido_daño, producido_curacion, creation_date, stats_all_id) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
        try {
            connection.setAutoCommit(false);
            preparedStatement = connection.prepareStatement(queryStats, PreparedStatement.RETURN_GENERATED_KEYS);
            preparedStatement.setInt(1, skill.getStats()[0]);
            preparedStatement.setInt(2, skill.getStats()[1]);
            preparedStatement.setInt(3, skill.getStats()[2]);
            preparedStatement.setInt(4, skill.getStats()[3]);
            preparedStatement.setInt(5, skill.getStats()[4]);
            preparedStatement.executeUpdate();

            ResultSet idGenerados = preparedStatement.getGeneratedKeys();
            idGenerados.next();
            int idStats = idGenerados.getInt(1);
            preparedStatement.close();

            preparedStatement = connection.prepareStatement(queryPersonaje);
            preparedStatement.setString(1, skill.getName());
            preparedStatement.setDouble(2, skill.getAlcance());
            preparedStatement.setString(3, skill.getDescription());
            preparedStatement.setInt(4, skill.getEnergyUse());
            preparedStatement.setInt(5, skill.getAmmoUse());
            preparedStatement.setInt(6, skill.getDamage());
            preparedStatement.setInt(7, skill.getHealing());
            preparedStatement.setDate(8, Date.valueOf(skill.getCreationDate()));
            preparedStatement.setInt(9, idStats);
            preparedStatement.executeUpdate();

            // Valida la transacción
            connection.commit();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (preparedStatement != null)
                try {
                    preparedStatement.close();
                    //resultado.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    public ResultSet consultaSeleccion(String consulta) throws SQLException{
        PreparedStatement sentencia = null;
        boolean verificacion = false;
        ResultSet resultSet = null;
        try {
            sentencia = connection.prepareStatement(consulta);
            verificacion = sentencia.execute();
        } catch (SQLException e) {
            e.printStackTrace();
            verificacion = false;
        }
        if (verificacion) {
            try {
                resultSet = sentencia.getResultSet();
            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            }
        }
        return resultSet;
    }

    public void actualizarValorTabla(String tabla, String campo, String valor, int id) {
        String consulta = "UPDATE " + tabla + " SET " + campo + " = ? WHERE id = ?;";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(consulta);
            if (Util.isIntegerParseInt(valor) != null) {
                int numero = Integer.parseInt(valor);
                preparedStatement.setInt(1, numero);
            } else if (Util.isDateParseDate(valor) != null) {
                preparedStatement.setDate(1, Date.valueOf(LocalDate.parse(valor)));
            } else {
                preparedStatement.setString(1, valor);
            }
            preparedStatement.setInt(2, id);
            preparedStatement.execute();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public String queryObtenerStats(String tabla, String nombre) {
        return "SELECT S.id FROM stats_all S, " + tabla + " X WHERE S.id = X.stats_all_id AND X.nombre = \"" + nombre + "\"";
    }

    public String queryObtenerBusqueda(int tablaIndex, String columna, String simbolo, String valor, boolean stats, boolean verificacionNombre) {
        String tabla = "";
        if (tablaIndex == 0) {
            tabla = "personajes";
        } else if (tablaIndex == 1) {
            tabla = "objetos";
        } else if (tablaIndex == 2) {
            tabla = "habilidades";
        }

        if (simbolo.equals("=")) {
            simbolo = "LIKE";
        }

        if (verificacionNombre) {
            return "SELECT X.* FROM " + tabla + " X WHERE X.nombre = " + "\"" + valor + "\"" + ";";
        }

//        if(valor.equals("")){
//            return "SELECT X.*, S.* FROM stats_all S, " + tabla + " X WHERE S.id = X.stats_all_id;";
//        } else
        if (!stats) {
            return "SELECT X.*, S.* FROM stats_all S, " + tabla + " X WHERE S.id = X.stats_all_id AND X." + columna + " " + simbolo + " " + "\"" + valor + "\"" + ";";

        } else {
            return "SELECT X.*, S.* FROM stats_all S, " + tabla + " X WHERE S.id = X.stats_all_id AND S." + columna + " " + simbolo + " " + "\"" + valor + "\"" + ";";

        }
    }

    public void dropRow(String tabla, String nombre) {
        PreparedStatement preparedStatement = null;
        String queryStats = "DELETE FROM stats_all WHERE id = (Select X.stats_all_id FROM " + tabla + " X , stats_all S WHERE  X.stats_all_id = S.id AND X.nombre = '" + nombre + "');";
        String queryObjeto = "DELETE FROM " + tabla + " WHERE nombre = '" + nombre + "';";

//        DELETE FROM stats_all WHERE id = (Select X.stats_all_id FROM personajes X , stats_all S WHERE  X.stats_all_id = S.id AND X.nombre = "Mixed");
//        DELETE FROM personajes WHERE nombre = "Mixed";
        try {
            preparedStatement = connection.prepareStatement(queryStats);
            preparedStatement.executeUpdate();

//            preparedStatement.close();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        }

        try {
            preparedStatement = connection.prepareStatement(queryObjeto);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }

//        return "DELETE FROM " + tabla + " X , stats_all S WHERE X.nombre = " + "\"" + nombre + "\" AND S.id = (Select X.stats_all_id FROM \"" + tabla + "\" X , stats_all S WHERE  X.stats_all_id = S.id AND X.nombre = \"" + nombre + "\");";
    }


    //    consultaSeleccion("Select X.stats_all_id FROM " + tabla + " X , stats_all S WHERE  X.stats_all_id = S.id AND X.nombre = " + "\"" + nombre + "\";").getMetaData()
    //    public String dataTypeColumn(String tabla, String columna) throws SQLException {
//        consultaSeleccion("SELECT DATA_TYPE FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '" + tabla + "' AND COLUMN_NAME = '" + columna + "';");
//    }
    public String getSgbd() {
        return sgbd;
    }

    public void setSgbd(String sgbd) {
        this.sgbd = sgbd;
    }

    public String getBbdd() {
        return bbdd;
    }

    public void setBbdd(String bbdd) {
        this.bbdd = bbdd;
    }

    public String getServidor() {
        return servidor;
    }

    public void setServidor(String servidor) {
        this.servidor = servidor;
    }

    public String getPuerto() {
        return puerto;
    }

    public void setPuerto(String puerto) {
        this.puerto = puerto;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getTipo() {
        return tipo;
    }

    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    public int getError() {
        return error;
    }

    public void setError(int error) {
        this.error = error;
    }

    public static String getDbMYSQL() {
        return dbMYSQL;
    }

    public static void setDbMYSQL(String dbMYSQL) {
        BaseDeDatos.dbMYSQL = dbMYSQL;
    }

    public static String getDbPostgreSQL() {
        return dbPostgreSQL;
    }

    public static void setDbPostgreSQL(String dbPostgreSQL) {
        BaseDeDatos.dbPostgreSQL = dbPostgreSQL;
    }

    public Connection getConnection() {
        return connection;
    }

    public void setConnection(Connection connection) {
        this.connection = connection;
    }
}
