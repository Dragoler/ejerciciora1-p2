package modelo;

import clases.Character;
import clases.Object;
import clases.Skill;
import clases.Usuario;
import org.w3c.dom.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.time.LocalDate;
import java.util.ArrayList;

/**
 * Clase modelo, donde estan todos los metodos para manejar los datos, crear otras clases, buscar, eliminar, verificar,
 * crear XML y cargarlo.
 */
public class Model {
    private ArrayList<Character> listCharacters;
    private ArrayList<Object> listObjects;
    private ArrayList<Skill> listSkills;
    private ArrayList<Usuario> listUsuarios;
    private String selecUser;

    /**
     * Constructor de la clase Modelo. Se crean e inicializan 3 arrays para cada uno de los objetos existentes.
     */
    public Model() {
        listCharacters = new ArrayList<Character>();
        listObjects = new ArrayList<Object>();
        listSkills = new ArrayList<Skill>();
        listUsuarios = new ArrayList<Usuario>();
    }

    public ArrayList<Character> getListCharacters() {
        return listCharacters;
    }

    public void setListCharacters(ArrayList<Character> listCharacters) {
        this.listCharacters = listCharacters;
    }

    public void setListsNew() {
        this.listCharacters = new ArrayList<Character>();
        this.listObjects = new ArrayList<Object>();
        this.listSkills = new ArrayList<Skill>();
    }


    public ArrayList<Object> getListObjects() {
        return listObjects;
    }

    public void setListObjects(ArrayList<Object> listObjects) {
        this.listObjects = listObjects;
    }

    public ArrayList<Skill> getListSkills() {
        return listSkills;
    }

    public void setListSkills(ArrayList<Skill> listSkills) {
        this.listSkills = listSkills;
    }

    public ArrayList<Usuario> getListUsuarios() {
        return listUsuarios;
    }

    public void setListUsuarios(ArrayList<Usuario> listUsuarios) {
        this.listUsuarios = listUsuarios;
    }

    /**
     * Metodo para crear el objeto Character. Devuelve un booelano para verificar si se ha podido crear o no.
     * @param name {@link String}
     * @param age {@link Integer}
     * @param alcance {@link Float}
     * @param biography {@link String}
     * @param force {@link Integer}
     * @param agility {@link Integer}
     * @param dextery {@link Integer}
     * @param perception {@link Integer}
     * @param inteligence {@link Integer}
     * @return Character
     */
    public boolean newCharacter(String name, int age, float alcance, String biography, int force, int agility, int dextery, int perception, int inteligence){
        if (verificationCharacter(name) == true){
            Character character = new Character(name, age, alcance, biography, force, agility, dextery, perception, inteligence);
            listCharacters.add(character);
            return true;
        }
        return false;
    }

    /**
     * Metodo para crear el objeto Object. Devuelve un booelano para verificar si se ha podido crear o no.
     * @param name {@link String}
     * @param description {@link String}
     * @param alcance {@link Float}
     * @param force {@link Integer}
     * @param agility {@link Integer}
     * @param dextery {@link Integer}
     * @param perception {@link Integer}
     * @param inteligence {@link Integer}
     * @return java.lang.Boolean
     */
    public boolean newObject(String name, String description, float alcance, int force, int agility, int dextery, int perception, int inteligence){
        if (verificationObject(name) == true){
            Object object = new Object(name, description, alcance, force, agility, dextery, perception, inteligence);
            listObjects.add(object);
            return true;
        }
        return false;
    }

    /**
     * Metodo para crear el objeto Skill. Devuelve un booelano para verificar si se ha podido crear o no.
     * @param name {@link String}
     * @param description {@link String}
     * @param energyUse {@link Integer}
     * @param ammoUse {@link Integer}
     * @param damage {@link Integer}
     * @param healing {@link Integer}
     * @param alcance {@link Float}
     * @param force {@link Integer}
     * @param agility {@link Integer}
     * @param dextery {@link Integer}
     * @param perception {@link Integer}
     * @param inteligence {@link Integer}
     * @return java.lang.Boolean
     */
    public boolean newSkill(String name, String description, int energyUse, int ammoUse, int damage, int healing, float alcance, int force, int agility, int dextery, int perception, int inteligence){
        if(verificacionSkill(name) == true){
            Skill skill = new Skill(name, description, energyUse, ammoUse, damage, healing, alcance, force, agility, dextery, perception, inteligence);
            listSkills.add(skill);
            return true;
        }
        return false;
    }

    /**
     * Metodos para buscar los objetos por el parametro nombre en el array de la clase objeto Character. Se usa en
     * cojunto a los metodos de crear y buscar para cercionarse de que un objeto con ese nombre exista ya o no.
     * @param name {@link String}
     * @return java.lang.Boolean
     */
    public Character findCharacter(String name){
        for (Character foundCharacter: listCharacters) {
            if(foundCharacter.getName().equals(name)){
                return foundCharacter;
            }
        }
        return null;
    }

    /**
     * Metodos para buscar los objetos por el parametro nombre en el array de la clase objeto Object. Se usa en
     * cojunto a los metodos de crear y buscar para cercionarse de que un objeto con ese nombre exista ya o no.
     * @param name {@link String}
     * @return
     */
    public Object findObject(String name){
        for (Object foundObjetc : listObjects) {
            if(foundObjetc.getName().equals(name)){
                return foundObjetc;
            }
        }
        return null;
    }

    /**
     * Metodos para buscar los objetos por el parametro nombre en el array de la clase objeto Skill. Se usa en
     * cojunto a los metodos de crear y buscar para cercionarse de que un objeto con ese nombre exista ya o no.
     * @param name {@link String}
     * @return
     */
    public Skill findSkill(String name){
        for (Skill foundSkill : listSkills) {
            if(foundSkill.getName().equals(name)){
                return foundSkill;
            }
        }
        return null;
    }

    /**
     * Metodo para eliminar un objeto existente. Devuelven un booelano para verificar si se ha podido borrar o no.
     * @param name {@link String}
     * @return
     */
    public boolean deleteCharacter(String name){
        Character foundDeleteCharacter = findCharacter(name);
        if (foundDeleteCharacter != null){
            listCharacters.remove(foundDeleteCharacter);
            return true;
        }
        return false;
    }

    /**
     * Metodo para eliminar un objeto existente. Devuelven un booelano para verificar si se ha podido borrar o no.
     * @param name {@link String}
     * @return
     */
    public  boolean deleteObject(String name){
        Object foundDeleteObject = findObject(name);
        if (foundDeleteObject != null){
            listObjects.remove(name);
            return true;
        }
        return false;
    }

    /**
     * Metodo para eliminar un objeto existente. Devuelven un booelano para verificar si se ha podido borrar o no.
     * @param name {@link String}
     * @return
     */
    public boolean deleteSkill(String name){
        Skill foundDeleteSkill = findSkill(name);
        if (foundDeleteSkill != null){
            listSkills.remove(foundDeleteSkill);
            return true;
        }
        return false;
    }

    /**
     * Metodos para modificar los objetos de la clase Character. Devuelven un booelano para verificar si se ha podido
     * modificar o no.
     * @param name {@link String}
     * @param age {@link Integer}
     * @param alcance {@link Float}
     * @param biography {@link String}
     * @param force {@link Integer}
     * @param agility {@link Integer}
     * @param dextery {@link Integer}
     * @param perception {@link Integer}
     * @param inteligence {@link Integer}
     * @return
     */
    public boolean modificateCharacter(String name, int age, float alcance, String biography, int force, int agility, int dextery, int perception, int inteligence){
        Character foundModificateCharacter = findCharacter(name);
        if (foundModificateCharacter != null){
            foundModificateCharacter.setName(name);
            foundModificateCharacter.setAge(age);
            foundModificateCharacter.setAlcance(alcance);
            foundModificateCharacter.setBiography(biography);
            foundModificateCharacter.setStats(new int[]{force,agility,dextery,perception,inteligence});
            return true;
        }
        return false;
    }

    /**
     * Metodos para modificar los objetos de la clase Object. Devuelven un booelano para verificar si se ha podido
     * modificar o no.
     * @param name {@link String}
     * @param alcance {@link Float}
     * @param description {@link String}
     * @param force {@link Integer}
     * @param agility {@link Integer}
     * @param dextery {@link Integer}
     * @param perception {@link Integer}
     * @param inteligence {@link Integer}
     * @return
     */
    public boolean modificateObject(String name, String description, float alcance, int force, int agility, int dextery, int perception, int inteligence){
        Object foundmodificateObject = findObject(name);
        if (foundmodificateObject != null){
            foundmodificateObject.setName(name);
            foundmodificateObject.setAlcance(alcance);
            foundmodificateObject.setDescription(description);
            foundmodificateObject.setStats(new int[]{force,agility,dextery,perception,inteligence});
            return true;
        }
        return false;
    }

    /**
     * Metodos para modificar los objetos de la clase Skill. Devuelven un booelano para verificar si se ha podido
     * modificar o no.
     * @param name {@link String}
     * @param description {@link String}
     * @param energyUse {@link Integer}
     * @param ammoUse {@link Integer}
     * @param damage {@link Integer}
     * @param healing {@link Integer}
     * @param alcance {@link Float}
     * @param force {@link Integer}
     * @param agility {@link Integer}
     * @param dextery {@link Integer}
     * @param perception {@link Integer}
     * @param inteligence {@link Integer}
     * @return
     */
    public boolean modificateSkill(String name, String description, int energyUse, int ammoUse, int damage, int healing, float alcance, int force, int agility, int dextery, int perception, int inteligence){
        Skill foundmodificateSkill = findSkill(name);
        if(foundmodificateSkill != null){
            foundmodificateSkill.setName(name);
            foundmodificateSkill.setDescription(description);
            foundmodificateSkill.setAlcance(alcance);
            foundmodificateSkill.setAmmoUse(ammoUse);
            foundmodificateSkill.setEnergyUse(energyUse);
            foundmodificateSkill.setDamage(damage);
            foundmodificateSkill.setHealing(healing);
            foundmodificateSkill.setStats(new int[]{force,agility,dextery,perception,inteligence});
            return true;
        }

        return false;
    }

    public boolean verificationCharacter(String name){
        if (findCharacter(name) != null){
            return false;
        }
        return true;
    }

    public boolean verificationObject(String name){
        if (findObject(name) != null){
            return false;
        }
        return true;
    }

    public Boolean verificacionSkill(String name){
        if (findSkill(name) != null){
            return false;
        }
        return true;
    }

    /**
     * Metodo completo para importar todos los objetos del programa a un XML
     * @param fichero {@link File}
     */
    public void importarXml(File fichero){
        ArrayList<Character> listCharactersImport = new ArrayList<>();
        ArrayList<Object> listObjectsImport = new ArrayList<>();
        ArrayList<Skill> listSkillsImport = new ArrayList<>();
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        Document documento = null;


        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            documento = builder.parse(fichero);

            // Recorre cada uno de los nodos
            NodeList lista = documento.getElementsByTagName("*");
            for(int i = 0 ; i<lista.getLength(); i++){
                Element nodoCharacter = (Element)lista.item(i);
                Element nodoObject = (Element)lista.item(i);
                Element nodoSkill = (Element)lista.item(i);
                if(nodoCharacter.getTagName().equals("character")){
                    Character importXmlCharacter = new Character();
                    importXmlCharacter.setAge(Integer.parseInt(nodoCharacter.getChildNodes().item(0).getTextContent()));
                    importXmlCharacter.setAlcance(Float.parseFloat(nodoCharacter.getChildNodes().item(1).getTextContent()));
                    importXmlCharacter.setName(nodoCharacter.getChildNodes().item(2).getTextContent());
                    importXmlCharacter.setBiography(nodoCharacter.getChildNodes().item(3).getTextContent());
                     importXmlCharacter.setStats(new int[] {
                             Integer.parseInt(nodoCharacter.getChildNodes().item(4).getTextContent()),
                             Integer.parseInt(nodoCharacter.getChildNodes().item(5).getTextContent()),
                             Integer.parseInt(nodoCharacter.getChildNodes().item(6).getTextContent()),
                             Integer.parseInt(nodoCharacter.getChildNodes().item(7).getTextContent()),
                             Integer.parseInt(nodoCharacter.getChildNodes().item(8).getTextContent())
                     });
                    importXmlCharacter.setCreationDate(LocalDate.parse(nodoCharacter.getChildNodes().item(9).getTextContent()));
                    listCharactersImport.add(importXmlCharacter);
                }
                if(nodoObject.getTagName().equals("object")){
                    Object importXmlObject = new Object();
                    importXmlObject.setName(nodoObject.getChildNodes().item(0).getTextContent());
                    importXmlObject.setDescription(nodoObject.getChildNodes().item(1).getTextContent());
                    importXmlObject.setAlcance(Float.parseFloat(nodoObject.getChildNodes().item(2).getTextContent()));
                    importXmlObject.setStats(new int[]{
                            Integer.parseInt(nodoObject.getChildNodes().item(3).getTextContent()),
                            Integer.parseInt(nodoObject.getChildNodes().item(4).getTextContent()),
                            Integer.parseInt(nodoObject.getChildNodes().item(5).getTextContent()),
                            Integer.parseInt(nodoObject.getChildNodes().item(6).getTextContent()),
                            Integer.parseInt(nodoObject.getChildNodes().item(7).getTextContent())
                    });
                    importXmlObject.setCreationDate(LocalDate.parse(nodoObject.getChildNodes().item(8).getTextContent()));
                    listObjectsImport.add(importXmlObject);
                }
                if(nodoSkill.getTagName().equals("skill")){
                    Skill importXmlSkill = new Skill();
                    importXmlSkill.setName(nodoSkill.getChildNodes().item(0).getTextContent());
                    importXmlSkill.setDescription(nodoSkill.getChildNodes().item(1).getTextContent());
                    importXmlSkill.setEnergyUse(Integer.parseInt(nodoSkill.getChildNodes().item(2).getTextContent()));
                    importXmlSkill.setAmmoUse(Integer.parseInt(nodoSkill.getChildNodes().item(3).getTextContent()));
                    importXmlSkill.setDamage(Integer.parseInt(nodoSkill.getChildNodes().item(4).getTextContent()));
                    importXmlSkill.setHealing(Integer.parseInt(nodoSkill.getChildNodes().item(5).getTextContent()));
                    importXmlSkill.setAlcance(Float.parseFloat(nodoSkill.getChildNodes().item(6).getTextContent()));
                    importXmlSkill.setStats(new int[]{
                            Integer.parseInt(nodoSkill.getChildNodes().item(7).getTextContent()),
                            Integer.parseInt(nodoSkill.getChildNodes().item(8).getTextContent()),
                            Integer.parseInt(nodoSkill.getChildNodes().item(9).getTextContent()),
                            Integer.parseInt(nodoSkill.getChildNodes().item(10).getTextContent()),
                            Integer.parseInt(nodoSkill.getChildNodes().item(11).getTextContent())
                    });
                    importXmlSkill.setCreationDate(LocalDate.parse(nodoSkill.getChildNodes().item(12).getTextContent()));
                    listSkillsImport.add(importXmlSkill);
                }
            }

        } catch (ParserConfigurationException pce) {
            pce.printStackTrace();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        } catch (org.xml.sax.SAXException e) {
            e.printStackTrace();
        }
        listCharacters = listCharactersImport;
        listObjects = listObjectsImport;
        listSkills = listSkillsImport;
    }

    /**
     * Metodo para exportar un documento XML y crear los objetos que halla en el mismo, metiendolos devidamente
     * en sus arrays y cargandolos en los modelos de los Jlist
     * @param fichero {@link File}
     */
    public void exportarXml(File fichero) {

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        Document documento = null;

        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            DOMImplementation dom = builder.getDOMImplementation();
            documento = dom.createDocument(null,  "xml", null);

            Element raiz = documento.createElement("elementos_juego");
            documento.getDocumentElement().appendChild(raiz);

            Element nodoCharacter = null, nodoObject = null, nodoSkill = null, nodoDatos = null;
            Text texto = null;

            for (Character exportXmlCharacter: listCharacters) {
                nodoCharacter = documento.createElement("character");
                raiz.appendChild(nodoCharacter);

                nodoDatos = documento.createElement("age");
                nodoCharacter.appendChild(nodoDatos);
                texto = documento.createTextNode("" + exportXmlCharacter.getAge());
                nodoDatos.appendChild(texto);

                nodoDatos = documento.createElement("alcance");
                nodoCharacter.appendChild(nodoDatos);
                texto = documento.createTextNode("" + exportXmlCharacter.getAlcance());
                nodoDatos.appendChild(texto);

                nodoDatos = documento.createElement("name");
                nodoCharacter.appendChild(nodoDatos);
                texto = documento.createTextNode("" + exportXmlCharacter.getName());
                nodoDatos.appendChild(texto);

                nodoDatos = documento.createElement("biography");
                nodoCharacter.appendChild(nodoDatos);
                texto = documento.createTextNode("" + exportXmlCharacter.getBiography());
                nodoDatos.appendChild(texto);

                nodoDatos = documento.createElement("stats_fuerza");
                nodoCharacter.appendChild(nodoDatos);
                texto = documento.createTextNode("" + Integer.toString(exportXmlCharacter.getStatsForIndex(0)));
                nodoDatos.appendChild(texto);
                nodoDatos = documento.createElement("stats_agilidad");
                nodoCharacter.appendChild(nodoDatos);
                texto = documento.createTextNode("" + Integer.toString(exportXmlCharacter.getStatsForIndex(1)));
                nodoDatos.appendChild(texto);
                nodoDatos = documento.createElement("stats_destreza");
                nodoCharacter.appendChild(nodoDatos);
                texto = documento.createTextNode("" + Integer.toString(exportXmlCharacter.getStatsForIndex(2)));
                nodoDatos.appendChild(texto);
                nodoDatos = documento.createElement("stats_percepcion");
                nodoCharacter.appendChild(nodoDatos);
                texto = documento.createTextNode("" + Integer.toString(exportXmlCharacter.getStatsForIndex(3)));
                nodoDatos.appendChild(texto);
                nodoDatos = documento.createElement("stats_inteligencia");
                nodoCharacter.appendChild(nodoDatos);
                texto = documento.createTextNode("" + Integer.toString(exportXmlCharacter.getStatsForIndex(4)));
                nodoDatos.appendChild(texto);
                nodoDatos = documento.createElement("creation_date");
                nodoCharacter.appendChild(nodoDatos);
                texto = documento.createTextNode(exportXmlCharacter.getCreationDate().toString());
                nodoDatos.appendChild(texto);
            }
            for (Object exportXmlObject : listObjects) {
                nodoObject = documento.createElement("object");
                raiz.appendChild(nodoObject);

                nodoDatos = documento.createElement("name");
                nodoObject.appendChild(nodoDatos);
                texto = documento.createTextNode("" + exportXmlObject.getName());
                nodoDatos.appendChild(texto);

                nodoDatos = documento.createElement("description");
                nodoObject.appendChild(nodoDatos);
                texto = documento.createTextNode("" + exportXmlObject.getDescription());
                nodoDatos.appendChild(texto);

                nodoDatos = documento.createElement("alcance");
                nodoObject.appendChild(nodoDatos);
                texto = documento.createTextNode("" + exportXmlObject.getAlcance());
                nodoDatos.appendChild(texto);

                nodoDatos = documento.createElement("stats_fuerza");
                nodoObject.appendChild(nodoDatos);
                texto = documento.createTextNode("" + exportXmlObject.getStatsForIndex(0));
                nodoDatos.appendChild(texto);
                nodoDatos = documento.createElement("stats_agilidad");
                nodoObject.appendChild(nodoDatos);
                texto = documento.createTextNode("" + Integer.toString(exportXmlObject.getStatsForIndex(1)));
                nodoDatos.appendChild(texto);
                nodoDatos = documento.createElement("stats_destreza");
                nodoObject.appendChild(nodoDatos);
                texto = documento.createTextNode("" + exportXmlObject.getStatsForIndex(2));
                nodoDatos.appendChild(texto);
                nodoDatos = documento.createElement("stats_percepcion");
                nodoObject.appendChild(nodoDatos);
                texto = documento.createTextNode("" + exportXmlObject.getStatsForIndex(3));
                nodoDatos.appendChild(texto);
                nodoDatos = documento.createElement("stats_inteligencia");
                nodoObject.appendChild(nodoDatos);
                texto = documento.createTextNode("" + exportXmlObject.getStatsForIndex(4));
                nodoDatos.appendChild(texto);

                nodoDatos = documento.createElement("creation_date");
                nodoObject.appendChild(nodoDatos);
                texto = documento.createTextNode(exportXmlObject.getCreationDate().toString());
                nodoDatos.appendChild(texto);
            }
            for (Skill exportXmlSkill : listSkills) {
                nodoSkill = documento.createElement("skill");
                raiz.appendChild(nodoSkill);

                nodoDatos = documento.createElement("name");
                nodoSkill.appendChild(nodoDatos);
                texto = documento.createTextNode("" + exportXmlSkill.getName());
                nodoDatos.appendChild(texto);

                nodoDatos = documento.createElement("description");
                nodoSkill.appendChild(nodoDatos);
                texto = documento.createTextNode("" + exportXmlSkill.getDescription());
                nodoDatos.appendChild(texto);

                nodoDatos = documento.createElement("energy_use");
                nodoSkill.appendChild(nodoDatos);
                texto = documento.createTextNode("" + exportXmlSkill.getEnergyUse());
                nodoDatos.appendChild(texto);

                nodoDatos = documento.createElement("ammo_use");
                nodoSkill.appendChild(nodoDatos);
                texto = documento.createTextNode("" + exportXmlSkill.getAmmoUse());
                nodoDatos.appendChild(texto);

                nodoDatos = documento.createElement("damage");
                nodoSkill.appendChild(nodoDatos);
                texto = documento.createTextNode("" + exportXmlSkill.getDamage());
                nodoDatos.appendChild(texto);

                nodoDatos = documento.createElement("healing");
                nodoSkill.appendChild(nodoDatos);
                texto = documento.createTextNode("" + exportXmlSkill.getHealing());
                nodoDatos.appendChild(texto);

                nodoDatos = documento.createElement("alcance");
                nodoSkill.appendChild(nodoDatos);
                texto = documento.createTextNode("" + exportXmlSkill.getAlcance());
                nodoDatos.appendChild(texto);

                nodoDatos = documento.createElement("stats_fuerza");
                nodoSkill.appendChild(nodoDatos);
                texto = documento.createTextNode("" + exportXmlSkill.getStatsForIndex(0));
                nodoDatos.appendChild(texto);
                nodoDatos = documento.createElement("stats_agilidad");
                nodoSkill.appendChild(nodoDatos);
                texto = documento.createTextNode("" + Integer.toString(exportXmlSkill.getStatsForIndex(1)));
                nodoDatos.appendChild(texto);
                nodoDatos = documento.createElement("stats_destreza");
                nodoSkill.appendChild(nodoDatos);
                texto = documento.createTextNode("" + exportXmlSkill.getStatsForIndex(2));
                nodoDatos.appendChild(texto);
                nodoDatos = documento.createElement("stats_percepcion");
                nodoSkill.appendChild(nodoDatos);
                texto = documento.createTextNode("" + exportXmlSkill.getStatsForIndex(3));
                nodoDatos.appendChild(texto);
                nodoDatos = documento.createElement("stats_inteligencia");
                nodoSkill.appendChild(nodoDatos);
                texto = documento.createTextNode("" + exportXmlSkill.getStatsForIndex(4));
                nodoDatos.appendChild(texto);

                nodoDatos = documento.createElement("creation_date");
                nodoSkill.appendChild(nodoDatos);
                texto = documento.createTextNode(exportXmlSkill.getCreationDate().toString());
                nodoDatos.appendChild(texto);
            }

            Source source = new DOMSource(documento);
            Result resultado = new StreamResult(fichero);

            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            transformer.transform(source, resultado);

        } catch (ParserConfigurationException pce) {
            pce.printStackTrace();
        } catch (TransformerConfigurationException tce) {
            tce.printStackTrace();
        } catch (TransformerException te) {
            te.printStackTrace();
        }   catch (NullPointerException npe){
            npe.printStackTrace();
        }
    }

    /**
     * Metodo para crear un documento SQL.
     * @param file {@link File}
     */
    public void exportarSQL(File file){
        FileWriter fileWriter = null;
        PrintWriter printWriter = null;
        try {
            fileWriter = new FileWriter(file , true);
            printWriter = new PrintWriter(fileWriter);
            for (Character characterSQL : listCharacters) {
                printWriter.println(characterSQL.toStringSQL());
            }
            for (Object objectSQL : listObjects) {
                printWriter.println(objectSQL.toStringSQL());
            }
            for (Skill skillSQL : listSkills) {
                printWriter.println(skillSQL.toStringSQL());
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (printWriter != null){
            printWriter.close();
        }
    }

    public void nuevoUsuario(String user, String password, int tipoUser) {
        Usuario usuario;
        if (tipoUser == 0) {
            usuario = new Usuario("", "", "", user, password, tipoUser);
        } else if (tipoUser == 1) {
            usuario = new Usuario("ra1db", "localhost", "3306", user, password, tipoUser);
        } else {
            usuario = new Usuario("", "", "", "", "", 2);
        }
        listUsuarios.add(usuario);
    }

    public Usuario findUser(String nombre) {
        for (Usuario foundUsuario : listUsuarios) {
            if (foundUsuario.getUsuario().equals(nombre)) {
                return foundUsuario;
            }
        }
        return null;
    }

    public String getSelecUser() {
        return selecUser;
    }

    public void setSelecUser(String selecUser) {
        this.selecUser = selecUser;
    }
}
