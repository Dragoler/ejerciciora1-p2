package vistas;

import clases.Usuario;
import modelo.Model;

import javax.swing.*;
import java.awt.event.*;

public class DataBaseConnection extends JDialog {
    private JPanel contentPane;
    private JButton buttonConectar;
    private JButton buttonCancel;
    public JComboBox cbTipoDB;
    public JPasswordField pfContrasena;
    public JTextField tfUsuario;
    public JTextField tfPuerto;
    public JTextField tfIp;
    public JTextField tfDB;
    private JLabel lDB;
    private JPanel lIp;
    private JLabel lPuerto;
    private JLabel lUsuario;
    private JLabel lContrasena;
    private JLabel lTipoDB;
    private boolean conect = false;
    private Model model;
    private Usuario usuario;

    public DataBaseConnection(Model model) {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonConectar);
        setLocationRelativeTo(null);
        model = model;
        usuario = model.findUser(model.getSelecUser());

        if (usuario.getTipoUsuario() == 0) {
            tfDB.setText(usuario.getBaseDeDatosUser());
            tfIp.setText(usuario.getIp());
            tfPuerto.setText(usuario.getPuerto());

            tfUsuario.setText(usuario.getUsuario());
            pfContrasena.setText(usuario.getContrasena());
            tfUsuario.setVisible(false);
            pfContrasena.setVisible(false);
            lUsuario.setVisible(false);
            lContrasena.setVisible(false);
        } else if (usuario.getTipoUsuario() == 1) {
            tfDB.setText(usuario.getBaseDeDatosUser());
            tfIp.setText(usuario.getIp());
            tfPuerto.setText(usuario.getPuerto());
            tfUsuario.setText(usuario.getUsuario());
            pfContrasena.setText(usuario.getContrasena());
            tfDB.setEditable(false);
            tfIp.setVisible(false);
            tfPuerto.setVisible(false);
            tfUsuario.setVisible(false);
            pfContrasena.setVisible(false);
            lIp.setVisible(false);
            lPuerto.setVisible(false);
            lUsuario.setVisible(false);
            lContrasena.setVisible(false);
        }
//        tfDB.setText("ra1db");
//        tfIp.setText("localhost");
//        tfPuerto.setText("3306");
//        tfUsuario.setText("ra1db");
//        pfContrasena.setText("1234");


        buttonConectar.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);

        pack();
    }

    private void onOK() {
        // add your code here
        conect = true;
        dispose();
    }

    private void onCancel() {
        // add your code here if necessary
        conect = false;
        dispose();
    }


    public boolean getConect() {
        return conect;
    }

    public void setConect(boolean conect) {
        this.conect = conect;
    }
}
