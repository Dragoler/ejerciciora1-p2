package vistas;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;

public class PrincipalView {
    public JPanel panel1;
    public JRadioButton rbCreateEdit;
    public JRadioButton rbList;
    public JComboBox cbObjetos;
    public JTextField tfName;
    public JTextArea taDescription;
    public JTextField tfFuerza;
    public JTextField tfAgilidad;
    public JTextField tfDestreza;
    public JTextField tfPercepcion;
    public JTextField tfInteligencia;
    public JTextField tfAge;
    public JTextField tfAlcance;
    public JTextField tfEnergyUse;
    public JTextField tfAmmoUse;
    public JTextField tfDamage;
    public JTextField tfHealing;
    public JButton bCreate;
    public JButton bDelete;
    public JButton bModificate;
    public JButton bListar;
    public JButton bSaveAs;
    public JButton bSave;
    public JButton bImport;
    public JButton bExport;
    public JButton bRoute;
    public JLabel lAge;
    public JCheckBox cbClean;
    public JLabel lNombre;
    public JLabel lDescription;
    public JLabel lStats;
    public JLabel lFuerza;
    public JLabel lAgilidad;
    public JLabel lDestreza;
    public JLabel lPercecion;
    public JLabel lInteligencia;
    public JLabel lAlcance;
    public JLabel lConsume;
    public JLabel lEnergia;
    public JLabel lMunicion;
    public JLabel lProduce;
    public JLabel lDamage;
    public JLabel lCuracion;
    public JPanel jp3;
    public JPanel jp2;
    public JPanel jp1;

    public JList lCharacters;
    public JList lSkills;
    public JList lObjects;
    public JTextArea taData;
    public JPanel jp3JL;
    public JPanel jp2TA;
    public JList lContent;
    public JPanel jpJLedit;
    public JTextField tfBuscar;
    public JLabel lBuscar;
    public JPanel jpBuscar;
    public JCheckBox cbAutoGuardar;
    public JLabel lGuardar;
    public JTable tDB;
    public JPanel jpTableDB;
    public JComboBox cbFindDB;
    public JTextField tfFindDB;
    public JComboBox cbFindValueDB;
    public JFrame frame;

    public JMenuBar barra;
    public JMenu login;
    public JMenuItem conectar;

    public DefaultTableModel dtm;

    /**
     * Constructor de la ventana principal. Establecera los estados por defecto en construccion a los diferentes
     * componentes de la ventana.
     */
    public PrincipalView() {
        frame = new JFrame("GestorPersonajesObjetosHabilidades");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        barra=new JMenuBar();
        login=new JMenu("Login");
        barra.add(login);
        conectar=new JMenuItem("Conectar");
        login.add(conectar);
        conectar.setActionCommand("Conectar");
        frame.setJMenuBar(barra);

        frame.pack();
        frame.setVisible(false);
        frame.setLocationRelativeTo(null);
        cbObjetos.addItem("Personaje/Mech");
        cbObjetos.addItem("Objeto");
        cbObjetos.addItem("Habilidad");
        rbCreateEdit.setSelected(true);
        cbClean.setSelected(true);
        cbAutoGuardar.setSelected(true);
        lGuardar.setText("No hay modificaciones");

        dtm = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                //return super.isCellEditable(row, column);
                if (column == 0) {
                    return false;
                }
                return true;
            }
        };

        tDB.setModel(dtm);

        lProduce.setVisible(false);
        lDamage.setVisible(false);
        tfDamage.setVisible(false);
        lCuracion.setVisible(false);
        tfHealing.setVisible(false);
        lConsume.setVisible(false);
        lMunicion.setVisible(false);
        tfAmmoUse.setVisible(false);
        lEnergia.setVisible(false);
        tfEnergyUse.setVisible(false);
        jpJLedit.setVisible(true);
        jp3JL.setVisible(false);
        jp2TA.setVisible(false);
        jpBuscar.setVisible(false);
        jpTableDB.setVisible(false);
    }


}