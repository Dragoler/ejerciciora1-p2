package vistas;

import clases.Usuario;
import modelo.Model;

import javax.swing.*;
import java.awt.event.*;

public class Login extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField tfUser;
    private JPasswordField pfPass;
    private JLabel lPass;
    private JLabel lUser;
    public JComboBox cbTipoUsuario;

    private String user;
    private String userController;
    private String pass;
    private String passController;
    private int verificate = 0;
    private int tipoUser;
    private int tipoUserController;
    private Model model;
    private Usuario usuario;

    /**
     *
     * @param verificateController - Establece la forma a proceder dependiendo de si el archivo de configuracion ha
     *                             podido ser o no cargado.
     */
    public Login(Model model, int verificateController) {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);
        setLocationRelativeTo(null);
        this.verificate = verificateController;

        this.model = model;

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);

        /*
         * Si el archivo de configuracion no existe, avisara de que los parametros introducidos a continuacion crearan
         * un nuevo usuario.
         */
        if(verificate == 3){
            JOptionPane.showMessageDialog(null, "Archivo de configuración no encontrado. Designe su usuario y contraseña " +
                    "\n ADVERTENCIA: Esta acción es irreversible, recuerde el usuario y contraseña que cree a continuación.");
        }

        pack();
        setVisible(true);
    }

    /**
     * Metodo por defecto del Jframe. Este metodo tiene 2 funciones.
     */
    private void onOK() {
        user  = tfUser.getText();
        pass = String.copyValueOf(pfPass.getPassword());

        /*
         * La primera, si no existe un archivo de configuracion, el controlador se lo indicara al Login, y le indicara
         * que tome por valor nuevo el usuario y contraseña que introduzca al usuario. Previamente el constructor
         * avisara al usuario de que los parametros que va a introducir crearan un nuevo usuario al no encontrar un
         * archivo de configuracion.
         */
        if(verificate == 3){
            user = tfUser.getText();
            pass = String.copyValueOf(pfPass.getPassword());
            tipoUser = cbTipoUsuario.getSelectedIndex();
            verificate = 1;
            dispose();
        }

        /*
         * La segunda funcion, es verificar si el usuario y contraseña pasados por el controlador, en caso de existir un
         * archivo de configuracion, son correctos. Si es asi, se accedera a la aplicacion. Si no es asi, se quedara en
         * bucle en el login hasta introducir los valores correctos o salir de la aplicacion.
         */
        else {
            //cbTipoUsuario.setSelectedIndex(tipoUserController);
            usuario = model.findUser(user);
            if (usuario != null) {
                if (pass.equals(usuario.getContrasena()) && usuario.getTipoUsuario() == cbTipoUsuario.getSelectedIndex()) {
                    verificate = 1;
                    model.setSelecUser(usuario.getUsuario());
                    dispose();
                } else {
                    verificate = 0;
                    JOptionPane.showMessageDialog(null, "El usuario, la contraseña o el tipo de usuario son incorrectos");
                }
            }
            else {
                verificate = 0;
                JOptionPane.showMessageDialog(null, "El usuario, la contraseña o el tipo de usuario son incorrectos");
            }
        }

    }

    private void onCancel() {
        verificate = 2;
        dispose();
    }

    /**
     * Establece el valor del usuario. Se hara uso de este metodo en caso de no existir el archivo de configuracion.
     * @param userController - Establece el valor de la variable del usuario.
     */
    public void setUserController(String userController) {
        this.userController = userController;
    }

    /**
     * Establece el valor de la contraseña. Se hara uso de este metodo en caso de no existir el archivo de configuracion.
     * @param passController - Establece el valor de la variable contraseña.
     */
    public void setPassController(String passController) {
        this.passController = passController;
    }

    /**
     * Devuelve el estado de la variable Verificate, que sirve para comprobar el estado del usuario y contraseña
     * introducidos asi como elegir la opcion de pasar los parametros por introduzca el usuario como usuarioy contraseña por defecto en caso de no cargar el archivo de configuracion.
     * @return Devuelve un Integer, para conocer si se ha logeado o no correctamente en el programa.
     */
    public int getVerificate() {
        return verificate;
    }

    /**
     * Devuelve el valor del usuario. Se hara uso de este metodo en caso para guardar la configuracion.
     * @return Devuelve el parametro usuario.
     */
    public String getUser() {
        return user;
    }

    /**
     * Devuelve el valor de la contraseña. Se hara uso de este metodo en caso para guardar la configuracion.
     * @return Devuelve el parametro contraseña.
     */
    public String getPass() {
        return pass;
    }

    public int getTipoUser() {
        return tipoUser;
    }

    public void setTipoUser(int tipoUser) {
        this.tipoUser = tipoUser;
    }
}
