package main;

import controlador.Controller;
import modelo.BaseDeDatos;
import modelo.Model;
import vistas.PrincipalView;

/**
 * Clase que inicializa el programa.
 */
public class Main {
    /**
     * Constructor principal del programa. Crea primero el modelo y la vista principal del programa para posteriormente
     * pasarselo al Controlador, que intercomunicara los metodos del Modelo con la vista pricipal del programa.
     * @param args {@link java.util.Arrays} {@link String}
     */
    public static void main (String[] args){
        PrincipalView principalView = new PrincipalView();
        Model model = new Model();
        BaseDeDatos baseDeDatos = new BaseDeDatos();
        Controller controller = new Controller(principalView, model, baseDeDatos);
    }
}
