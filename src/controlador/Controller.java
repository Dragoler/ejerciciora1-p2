package controlador;

import clases.Character;
import clases.Object;
import clases.Skill;
import clases.Usuario;
import modelo.BaseDeDatos;
import modelo.Model;
import util.Constantes;
import util.UsuarioCreacion;
import util.Util;
import vistas.DataBaseConnection;
import vistas.Login;
import vistas.PrincipalView;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.TableColumnModel;
import java.awt.event.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;

/**
 * @author Pablo
 * Clase controladora, donde confluyen los metodos internos que interactúan con los parametros de la interfaz obtenidos.
 */
public class Controller implements ActionListener, KeyListener, ListSelectionListener, WindowListener, TableModelListener {
    private PrincipalView principalView;
    private Model model;
    private BaseDeDatos baseDeDatos;
    private Usuario usuario;
    private UsuarioCreacion usuarioCreacion;
    private DefaultListModel<Character> lCharacter;
    private DefaultListModel<Object> lObject;
    private DefaultListModel<Skill> lSkill;
    private File ficheroPrograma= new File("gestor");
    private File ruta = new File("fichero.conf");
    private File SQL = new File ("testSQL.sql");
    private boolean verification;
    private String selected;
    private boolean modificate = false;
    private String user;
    private String password;
    private int tipoUser;
    private String errorMensaje;
    private boolean conectado = false;
    private int[] index;
    JFileChooser jFileChooser;
    FileNameExtensionFilter fileNameExtensionFilter;

    /**
     * Crea el controlador del programa, donde confluyen los metodos de datos con los datos obtenidos en la interfaz.
     * Carga los listener de los componentes de la ventana principal del programa.
     * Creara la ventana login y le pasara parametros, a la par que recivira de ella el estado de la verificacion de
     * login.
     * @param principalView - Recive la ventana principal del programa.
     * @param model - Recive el modelo donde estan todos los metodos de uso de datos y funciones del programa.
     */
    public Controller(PrincipalView principalView, Model model, BaseDeDatos baseDeDatos1) {
        this.principalView= principalView;
        this.model = model;
        this.baseDeDatos = baseDeDatos1;
        Login login;
        lCharacter = new DefaultListModel<Character>();
        lObject = new DefaultListModel<Object>();
        lSkill = new DefaultListModel<Skill>();
        anadirActionListener(this);
        anadirKeyListener(this);
        anadirlistSelectionListener(this);
        anadirWindowListener(this);
        listarCharacters();
        iniciarTableMoldeListener(this);
        usuarioCreacion = new UsuarioCreacion(model);

        /**
         * Al crear el controlador, el programa cargara por defecto la ruta de configuracion (que incluye usuario,
         * contraseña y ruta por defecto del XML). Creara la ventana Login y le pasara por parametros el estado de
         * carga de la configuracion, el usuario y la contraseña. En caso de no existir la ruta del XML del archivo
         * de configuracion, pondra la ruta por defecto. Cargara el XML con la ruta pasada (guardada en la
         * configuracion o la de por defecto dependiendo del caso). En caso de que el usuario cancele el loguearse,
         * cerrara el programa.
         */
        if(ruta.exists()){
            cargarFicheroConfiguracion();
            login = new Login(model, 0);
            if(login.getVerificate() == 1){
               principalView.frame.setVisible(true);
               if(ficheroPrograma.exists()){
                   model.importarXml(ficheroPrograma);
                   listarSkills();
                   listarObjects();
                   listarCharacters();
               } else {
                   ficheroPrograma= new File("gestor");
               }
            }
            else if(login.getVerificate() == 2){
                System.exit(0);
            }
        }

        /*
         * Si la ruta de configuracion no existe, le indicara al constructor del login que debe establecer el usuario
         * y contraseña que ponga el usuario, y avisarle de tal hecho. Tambien pondra la ruta de carga del XML por
         * defecto y lo cargara en caso de existir un XML en dicha ruta. En caso de que el usuario cancele el loguearse,
         * cerrara el programa.
         */
        else if(!ruta.exists()){
            user = null;
            password = null;
            login = new Login(model, 3);
            if(login.getVerificate() == 1){
//                user = login.getUser();
//                password = login.getPass();
//                tipoUser = login.getTipoUser();
                ruta = new File("fichero.conf");
                ficheroPrograma= new File("gestor");
//                model.nuevoUsuario(user, password, tipoUser);
                guardarFicheroConfiguracion();
                principalView.frame.setVisible(true);
                if(ficheroPrograma.exists()){
                    model.importarXml(ficheroPrograma);
                    listarSkills();
                    listarObjects();
                    listarCharacters();
                }
            } else if (login.getVerificate() == 2){
                System.exit(0);
            }
        }

        try {
            usuario = model.findUser(model.getSelecUser());
            baseDeDatos = new BaseDeDatos("ra1db", "localhost", "3306", usuario.getUsuario(), usuario.getContrasena(), 0);
            baseDeDatos.conectar();
            if (!baseDeDatos.estadoConexion()) {
                JOptionPane.showMessageDialog(null, "No se ha podido realizar la conexion: ");
                principalView.rbCreateEdit.setSelected(true);
            } else {
                tipeUser(usuario.getTipoUsuario());
                JOptionPane.showMessageDialog(null, "Conectado a la Base de Datos");
                principalView.conectar.setText("Desconectar");
                principalView.rbCreateEdit.setSelected(true);
                conectado = true;

                model.setListsNew();
                lCharacter = new DefaultListModel<Character>();
                lObject = new DefaultListModel<Object>();
                lSkill = new DefaultListModel<Skill>();

                principalView.cbObjetos.setSelectedIndex(0);
                principalView.rbList.setEnabled(false);

                onlyCreate();
//                especificarTabla(Constantes.queryListarCharactersAndStats, false);
            }
        } catch (SQLException e1) {
            e1.printStackTrace();
        }
    }

    private void anadirActionListener(ActionListener listener) {
        principalView.bCreate.addActionListener(listener);
        principalView.bListar.addActionListener(listener);
        principalView.bExport.addActionListener(listener);
        principalView.bImport.addActionListener(listener);
        principalView.bDelete.addActionListener(listener);
        principalView.bModificate.addActionListener(listener);
        principalView.bRoute.addActionListener(listener);
        principalView.bSave.addActionListener(listener);
        principalView.bSaveAs.addActionListener(listener);
        principalView.rbList.addActionListener(listener);
        principalView.rbCreateEdit.addActionListener(listener);
        principalView.cbObjetos.addActionListener(listener);
        principalView.conectar.addActionListener(listener);
        principalView.cbFindValueDB.addActionListener(listener);
        principalView.cbFindDB.addActionListener(listener);
    }

    private void anadirKeyListener(KeyListener listener){
        principalView.tfBuscar.addKeyListener(listener);
        principalView.tfFindDB.addKeyListener(listener);
    }

    private void anadirlistSelectionListener (ListSelectionListener listSelectionListener){
        principalView.lContent.addListSelectionListener(listSelectionListener);
        principalView.lCharacters.addListSelectionListener(listSelectionListener);
        principalView.lObjects.addListSelectionListener(listSelectionListener);
        principalView.lSkills.addListSelectionListener(listSelectionListener);
    }

    private void anadirWindowListener(WindowListener windowListener){
        principalView.frame.addWindowListener(windowListener);
    }

    private void iniciarTableMoldeListener(TableModelListener tableModelListener) {
        principalView.dtm.addTableModelListener(tableModelListener);
    }

    /**
     * Metodo que detecta las acciones de los elementos de la interfaz (Pulsacion, seleccion, etc). En este metodo se
     * llevara a cabo el proceso logico de acciones dependiendo de las elecciones tomadas por el usuario (Crear, borrar,
     * cambiar a lista, etc).
     * @param e - Recive la accion que se lleva a cabo en la interfaz.
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        String actionCommand = e.getActionCommand();

        /*
         * Reconoce las acciones del Checkbox entre sus 2 opciones. Con ello, cargara unos elementos en la interfaz u
         * otros dependiendo de la eleccion del usuario.
         */
        switch (actionCommand){
            case "Conectar":
                if (!conectado) {
                    if (!baseDeDatos.estadoConexion()) {
                        DataBaseConnection dataBaseConnection = new DataBaseConnection(model);
                        dataBaseConnection.setVisible(true);
                        if (dataBaseConnection.getConect()) {
                            try {
                                baseDeDatos = new BaseDeDatos(dataBaseConnection.tfDB.getText().toString(), dataBaseConnection.tfIp.getText().toString(), dataBaseConnection.tfPuerto.getText().toString(), dataBaseConnection.tfUsuario.getText().toString(), String.valueOf(dataBaseConnection.pfContrasena.getPassword()), dataBaseConnection.cbTipoDB.getSelectedIndex());
                                baseDeDatos.conectar();
                            /*revisar los tipos de errores*/
                                if (!baseDeDatos.estadoConexion()) {
                                    JOptionPane.showMessageDialog(null, "No se ha podido realizar la conexion");
                                    principalView.rbCreateEdit.setSelected(true);
                                } else {
                                    JOptionPane.showMessageDialog(null, "Conectado de la Base de Datos");
                                    principalView.conectar.setText("Desconectar");
                                    principalView.rbCreateEdit.setSelected(true);
                                    conectado = true;

                                    model.setListsNew();
                                    lCharacter = new DefaultListModel<Character>();
                                    lObject = new DefaultListModel<Object>();
                                    lSkill = new DefaultListModel<Skill>();

                                    principalView.cbObjetos.setSelectedIndex(0);
                                    principalView.rbList.setEnabled(false);
                                }
                            } catch (SQLException e1) {
                                e1.printStackTrace();
                            }
                        }
                    }
                } else {
                    try {
                        baseDeDatos.desconectar();
                        JOptionPane.showMessageDialog(null, "Desconectado de la Base de Datos");
                        principalView.rbList.setEnabled(true);
                    } catch (SQLException e1) {
                        e1.printStackTrace();
                    }
                    principalView.conectar.setText("Conectar");
                    conectado = false;
                    principalView.rbCreateEdit.setSelected(true);

                    model.setListsNew();
                    lCharacter = new DefaultListModel<Character>();
                    lObject = new DefaultListModel<Object>();
                    lSkill = new DefaultListModel<Skill>();

                    if (ficheroPrograma.exists()) {
                        model.importarXml(ficheroPrograma);
                        listarSkills();
                        listarObjects();
                        listarCharacters();
                    } else {
                        ficheroPrograma = new File("gestor");
                    }
                }

            case "Crear/Editar":
                //cleantf();

//                elementsCreate_Edit();
                tipeUser(usuario.getTipoUsuario());
                onlyCreate();

                break;
            case "Mostrar":
                //cleantf();
                elementsList();
                onlyLists();
                principalView.lCharacters.setModel(lCharacter);
                principalView.lObjects.setModel(lObject);
                principalView.lSkills.setModel(lSkill);
                break;
            default:
                break;
        }

        /*
         * Reconoce los botones pulsados y les asocia unas acciones concretas.
         */
        switch (actionCommand) {

            /*
             * El boton Crear verificara que tipo de objeto esta seleccionado en el combobox de los 3 disponibles.
             * Despues, llamara a un metodo que verificara que estan escritos todos los valores obligatorios.
             * Posteriormente llamara al metodo de creacion del objeto correspondiente el cual le devolvera un true o un
             * false en caso de poder crear o no el objeto (No se pueden repetir nombres). En caso de poder crearlo,
             * lo añadira al modelo del Jlist del objeto correspondiente y lo cargara en el Jlist inferior de la
             * interfaz dispuesto para ello. Por ultimo, llamara al metodo para verificar si el guardado automatico esta
             * activo o no, para que lo guarde o avise de que hay cambios sin guardar.
             */
            case "Crear":
                //principalView.lContent.clearSelection();
                if(principalView.cbObjetos.getSelectedIndex() == 0){
                    if(contentVerificate0() != true){
                        JOptionPane.showMessageDialog(null,"Introduzca todos los valores");
                    } else {
                        if (baseDeDatos.estadoConexion()) {
                            if (verificacionNombreBaseDeDatos(principalView.tfName.getText().toString())) {
                                JOptionPane.showMessageDialog(null, "Nombre en uso");
                            } else {
                                verification = model.newCharacter(principalView.tfName.getText().toString(), Integer.parseInt(principalView.tfAge.getText()),
                                        Float.parseFloat(principalView.tfAlcance.getText()), principalView.taDescription.getText().toString(),
                                        Integer.parseInt(principalView.tfFuerza.getText()), Integer.parseInt(principalView.tfAgilidad.getText()),
                                        Integer.parseInt(principalView.tfDestreza.getText()), Integer.parseInt(principalView.tfPercepcion.getText()),
                                        Integer.parseInt(principalView.tfInteligencia.getText()));
                                baseDeDatos.upCharacter(model.findCharacter(principalView.tfName.getText().toString()));
                            }
                            especificarTabla(Constantes.queryListarCharactersAndStats, false);
                        } else {
                            verification = model.newCharacter(principalView.tfName.getText().toString(), Integer.parseInt(principalView.tfAge.getText()),
                                    Float.parseFloat(principalView.tfAlcance.getText()), principalView.taDescription.getText().toString(),
                                    Integer.parseInt(principalView.tfFuerza.getText()), Integer.parseInt(principalView.tfAgilidad.getText()),
                                    Integer.parseInt(principalView.tfDestreza.getText()), Integer.parseInt(principalView.tfPercepcion.getText()),
                                    Integer.parseInt(principalView.tfInteligencia.getText()));
                            if (verification != true) {
                                JOptionPane.showMessageDialog(null, "Nombre en uso");
                            } else {
                                listarCharacters();
                                if (!conectado) {
                                    autoguardado();
                                }

                            }
                        }
                    }
                } else if (principalView.cbObjetos.getSelectedIndex() == 1){
                    if(contentVerificate1() !=true){
                        JOptionPane.showMessageDialog(null,"Introduzca todos los valores");
                    } else {
                        if (baseDeDatos.estadoConexion()) {
                            if (verificacionNombreBaseDeDatos(principalView.tfName.getText().toString())) {
                                JOptionPane.showMessageDialog(null, "Nombre en uso");
                            } else {
                                verification = model.newObject(principalView.tfName.getText().toString(), principalView.taDescription.getText().toString(),
                                        Float.parseFloat(principalView.tfAlcance.getText()),
                                        Integer.parseInt(principalView.tfFuerza.getText()), Integer.parseInt(principalView.tfAgilidad.getText()),
                                        Integer.parseInt(principalView.tfDestreza.getText()), Integer.parseInt(principalView.tfPercepcion.getText()),
                                        Integer.parseInt(principalView.tfInteligencia.getText()));
                                baseDeDatos.upObject(model.findObject(principalView.tfName.getText().toString()));
                            }
                            especificarTabla(Constantes.queryListarObjectsAndStats, false);
                        } else {
                            verification = model.newObject(principalView.tfName.getText().toString(), principalView.taDescription.getText().toString(),
                                    Float.parseFloat(principalView.tfAlcance.getText()),
                                    Integer.parseInt(principalView.tfFuerza.getText()), Integer.parseInt(principalView.tfAgilidad.getText()),
                                    Integer.parseInt(principalView.tfDestreza.getText()), Integer.parseInt(principalView.tfPercepcion.getText()),
                                    Integer.parseInt(principalView.tfInteligencia.getText()));


                            if (verification != true) {
                                JOptionPane.showMessageDialog(null, "Nombre en uso");
                            } else {
                                listarObjects();
                                if (!conectado) {
                                    autoguardado();
                                }

                            }
                        }
                    }
                } else if (principalView.cbObjetos.getSelectedIndex() == 2) {
                    if(contentVerificate2() != true){
                        JOptionPane.showMessageDialog(null,"Introduzca todos los valores");
                    } else {
                        if (baseDeDatos.estadoConexion()) {
                            if (verificacionNombreBaseDeDatos(principalView.tfName.getText().toString())) {
                                JOptionPane.showMessageDialog(null, "Nombre en uso");
                            } else {
                                verification = model.newSkill(principalView.tfName.getText().toString(), principalView.taDescription.getText().toString(),
                                        Util.isIntegerParseInt(principalView.tfEnergyUse.getText().toString()), Util.isIntegerParseInt(principalView.tfAmmoUse.getText().toString()), Util.isIntegerParseInt(principalView.tfDamage.getText().toString()), Util.isIntegerParseInt(principalView.tfHealing.getText().toString()),
                                        Float.parseFloat(principalView.tfAlcance.getText()),
                                        Integer.parseInt(principalView.tfFuerza.getText()), Integer.parseInt(principalView.tfAgilidad.getText()),
                                        Integer.parseInt(principalView.tfDestreza.getText()), Integer.parseInt(principalView.tfPercepcion.getText()),
                                        Integer.parseInt(principalView.tfInteligencia.getText()));
                                baseDeDatos.upSkill(model.findSkill(principalView.tfName.getText().toString()));
                            }
                            especificarTabla(Constantes.queryListarSkillsAndStats, false);
                        } else {
                            verification = model.newSkill(principalView.tfName.getText().toString(), principalView.taDescription.getText().toString(),
                                    Util.isIntegerParseInt(principalView.tfEnergyUse.getText().toString()), Util.isIntegerParseInt(principalView.tfAmmoUse.getText().toString()), Util.isIntegerParseInt(principalView.tfDamage.getText().toString()), Util.isIntegerParseInt(principalView.tfHealing.getText().toString()),
                                    Float.parseFloat(principalView.tfAlcance.getText()),
                                    Integer.parseInt(principalView.tfFuerza.getText()), Integer.parseInt(principalView.tfAgilidad.getText()),
                                    Integer.parseInt(principalView.tfDestreza.getText()), Integer.parseInt(principalView.tfPercepcion.getText()),
                                    Integer.parseInt(principalView.tfInteligencia.getText()));


                            if (verification != true) {
                                JOptionPane.showMessageDialog(null, "Nombre en uso");
                            } else {
                                listarSkills();
                                if (!conectado) {
                                    autoguardado();
                                }

                            }
                        }
                    }
                }
                break;

            /*
             * Seguira los mismos pasos que el metodo crear, con la salvedad que en vez de llamar al metodo de creacion
             * de objeto, llamara al de moficiarlo. Este le devolvera un booleano para indicarle si ha podido
             * modificarlo o no existe el objeto a modificar.
             */
            case "Modificar":
                if(principalView.cbObjetos.getSelectedIndex() == 0){
                    if(contentVerificate0() !=true){
                        JOptionPane.showMessageDialog(null,"Introduzca todos los valores");
                    } else {
                        verification = model.modificateCharacter(principalView.tfName.getText().toString(),Integer.parseInt(principalView.tfAge.getText()),
                                Float.parseFloat(principalView.tfAlcance.getText()), principalView.taDescription.getText().toString(),
                                Integer.parseInt(principalView.tfFuerza.getText()), Integer.parseInt(principalView.tfAgilidad.getText()),
                                Integer.parseInt(principalView.tfDestreza.getText()), Integer.parseInt(principalView.tfPercepcion.getText()),
                                Integer.parseInt(principalView.tfInteligencia.getText()));
                        if(verification !=true){
                            JOptionPane.showMessageDialog(null,"No existe el Personaje");
                        } else{
                            listarCharacters();
                            autoguardado();
                        }
                    }
                } else if (principalView.cbObjetos.getSelectedIndex() == 1){
                    if(contentVerificate1() !=true){
                        JOptionPane.showMessageDialog(null,"Introduzca todos los valores");
                    } else {
                        verification = model.modificateObject(principalView.tfName.getText().toString(), principalView.taDescription.getText().toString(),
                                Float.parseFloat(principalView.tfAlcance.getText()),
                                Integer.parseInt(principalView.tfFuerza.getText()), Integer.parseInt(principalView.tfAgilidad.getText()),
                                Integer.parseInt(principalView.tfDestreza.getText()), Integer.parseInt(principalView.tfPercepcion.getText()),
                                Integer.parseInt(principalView.tfInteligencia.getText()));
                        if(verification !=true){
                            JOptionPane.showMessageDialog(null,"No existe el Objeto");
                        } else{
                            listarObjects();
                            autoguardado();
                        }
                    }
                } else if (principalView.cbObjetos.getSelectedIndex() == 2) {
                    if(contentVerificate2() != true){
                        JOptionPane.showMessageDialog(null,"Introduzca todos los valores");
                    } else {
                        verification = model.modificateSkill(principalView.tfName.getText().toString(), principalView.taDescription.getText().toString(),
                                Util.isIntegerParseInt(principalView.tfEnergyUse.getText().toString()), Util.isIntegerParseInt(principalView.tfAmmoUse.getText().toString()), Util.isIntegerParseInt(principalView.tfDamage.getText().toString()), Util.isIntegerParseInt(principalView.tfHealing.getText().toString()),
                                Float.parseFloat(principalView.tfAlcance.getText()),
                                Integer.parseInt(principalView.tfFuerza.getText()), Integer.parseInt(principalView.tfAgilidad.getText()),
                                Integer.parseInt(principalView.tfDestreza.getText()), Integer.parseInt(principalView.tfPercepcion.getText()),
                                Integer.parseInt(principalView.tfInteligencia.getText()));
                        if(verification !=true){
                            JOptionPane.showMessageDialog(null,"No existe la habilidad");
                        } else{
                            listarSkills();
                            autoguardado();
                        }
                    }
                }
                break;
            case "SQL":
                model.exportarSQL(SQL);
                break;

            /*
             * Seguira los mismos pasos que el metodo crear, con la salvedad que en el metodo de crear objeto llamara
             * al metodo de borrarlo. En caso de no existir el objeto a borrar, devolvera un aviso.
             */
            case "Borrar":
                if (baseDeDatos.estadoConexion()) {
                    if (principalView.tDB.getSelectedRow() == -1) {
                        JOptionPane.showMessageDialog(null, "Seleccione un componente a borrar");
                    } else {
                        String tabla = "";
                        String tablaMostrar = "";
                        if (principalView.cbObjetos.getSelectedIndex() == 0) {
                            tabla = "personajes";
                            tablaMostrar = Constantes.queryListarCharactersAndStats;
                        } else if (principalView.cbObjetos.getSelectedIndex() == 1) {
                            tabla = "objetos";
                            tablaMostrar = Constantes.queryListarObjectsAndStats;
                        } else if (principalView.cbObjetos.getSelectedIndex() == 2) {
                            tabla = "habilidades";
                            tablaMostrar = Constantes.queryListarSkillsAndStats;
                        }
                        ResultSet test = null;
                        baseDeDatos.dropRow(tabla, (String) principalView.dtm.getValueAt(principalView.tDB.getSelectedRow(), 1));

                        especificarTabla(tablaMostrar, false);
                    }
                } else {
                    if (principalView.cbObjetos.getSelectedIndex() == 0) {
                        if (principalView.tfName.getText().equals("")) {
                            JOptionPane.showMessageDialog(null, "Introduzca un nombre por el que buscar, por favor.");
                        } else {
                            verification = model.deleteCharacter(principalView.tfName.getText().toString());
                            if (verification != true) {
                                JOptionPane.showMessageDialog(null, "El Personaje no existe.");
                            } else {
                                listarCharacters();
                                autoguardado();
                            }
                        }
                    } else if (principalView.cbObjetos.getSelectedIndex() == 1) {
                        if (principalView.tfName.getText().equals("")) {
                            JOptionPane.showMessageDialog(null, "Introduzca un nombre por el que buscar, por favor.");
                        } else {
                            verification = model.deleteObject(principalView.tfName.getText().toString());
                            if (verification != true) {
                                JOptionPane.showMessageDialog(null, "El Objeto no existe.");
                            } else {
                                listarObjects();
                                autoguardado();
                            }
                        }
                    } else if (principalView.cbObjetos.getSelectedIndex() == 2) {
                        if (principalView.tfName.getText().equals("")) {
                            JOptionPane.showMessageDialog(null, "Introduzca un nombre por el que buscar, por favor.");
                        } else {
                            verification = model.deleteSkill(principalView.tfName.getText().toString());
                            if (verification != true) {
                                JOptionPane.showMessageDialog(null, "La Habilidad no existe.");
                            } else {
                                listarSkills();
                                autoguardado();
                            }
                        }
                    }
                }
                break;

            /*
             * Boton que permitira escoger al usuario la localizacion donde guardara el archivo XML, lo pondra como
             * ruta por defecto y lo guardara.
             */
            case "Guardar como...":
                if (baseDeDatos.estadoConexion()) {
                    prepareDBXML();
                    userFichero();
                    JOptionPane.showMessageDialog(null, "Se ha exportado su base de datos a un fichero XML.");
                } else {
                    userFichero();
                }
                principalView.lGuardar.setText("Cambios guardados");
                modificate = true;
                break;

            /*
             *Boton que guarda de forma manual los objetos que tiene recogidos el programa en formato XML. Informa
             * de que no existen cambios por guardar en caso de tener desactivado el boton de autoguardado.
             */
            case "Guardar":
                model.exportarXml(ficheroPrograma);
                principalView.lGuardar.setText("Cambios guardados");
                modificate = false;
                break;

            /*
             * Boton que importa el XML desde una ruta fija y carga los Jlist con los elementos.
             */
            case "Importar":
                model.importarXml(ficheroPrograma);
                if(principalView.cbObjetos.getSelectedIndex() == 0){
                    listarCharacters();
                } else if(principalView.cbObjetos.getSelectedIndex() == 1){
                    listarObjects();
                } else if(principalView.cbObjetos.getSelectedIndex() == 3){
                    listarSkills();
                }
                modificate = true;

                break;

            /*
             * Boton que exporta a XML los objetos que tiene recogidos el programa.
             */
            case "Exportar":
                model.exportarXml(ficheroPrograma);
                modificate = false;
                break;

            /*
             * Cambia la ruta del archivo XML a la ruta por defecto.
             */
            case "Ruta por defecto":
                ficheroPrograma= new File("gestor");
                JOptionPane.showMessageDialog(null, "Cambiado la ruta de guardado del XML a la ruta por defecto");
                break;
        }

        switch (actionCommand) {
            case "comboBoxChanged":
                /*
                 * Switch que cambia los elementos en pantalla dependiendo del objeto del combobox seleccionado y limpia los textfield.
                 */
                switch (principalView.cbObjetos.getSelectedIndex()) {
                    case 0:
                        elementsCB1();
                        if (baseDeDatos.estadoConexion()) {
                            especificarTabla(Constantes.queryListarCharactersAndStats, false);
                        } else {
                            principalView.lContent.setModel(lCharacter);
                            listarCharacters();
                        }
                        if (principalView.cbClean.isSelected()) {
                            cleantf();
                        }
                        break;
                    case 1:
                        elementsCB2();
                        if (baseDeDatos.estadoConexion()) {
                            especificarTabla(Constantes.queryListarObjectsAndStats, false);
                        } else {
                            principalView.lContent.setModel(lObject);
                            listarObjects();
                        }
                        if (principalView.cbClean.isSelected()) {
                            cleantf();
                        }
                        break;
                    case 2:
                        elementsCB3();
                        if (baseDeDatos.estadoConexion()) {
                            especificarTabla(Constantes.queryListarSkillsAndStats, false);
                        } else {
                            principalView.lContent.setModel(lSkill);
                            listarSkills();
                        }
                        if (principalView.cbClean.isSelected()) {
                            cleantf();
                        }
                        break;
                    default:
                        break;
                }
                break;
            case "comboBoxChangedDB":
                if (principalView.cbFindDB.getSelectedItem() != null && principalView.tfFindDB.getText() != null && !principalView.tfFindDB.getText().equals("")) {
                    busquedaDB();
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {

    }

    /**
     * Metodo que recoge cada vez que se finaliza una pulsacion. Llamara a los metodos de buscar por 2 parametros
     * para cada uno de los tipos de objeto.
     * @param e - Recive el parametro de la tecla que ha sido pulsada y ya, se ha soltado.
     */
    @Override
    public void keyReleased(KeyEvent e) {
        if (principalView.rbList.isSelected()) {
            buscarCharacter(principalView.tfBuscar.getText());
            buscarObject(principalView.tfBuscar.getText());
            buscarSkill(principalView.tfBuscar.getText());
        } else if (baseDeDatos.estadoConexion()) {
            busquedaDB();
        }
    }

    /**
     * Metodo para mostrar componentes u ocultar en pantalla cuando se va a editar.
     */
    private void elementsCreate_Edit(){
        principalView.bCreate.setEnabled(true);
        principalView.bModificate.setEnabled(true);
        principalView.bDelete.setEnabled(true);
    }

    /**
     * Metodo para mostrar componentes u ocultar en pantalla cuando se va a listar.
     */
    private void elementsList(){
        principalView.bCreate.setEnabled(false);
        principalView.bModificate.setEnabled(false);
        principalView.bDelete.setEnabled(false);
    }

    /**
     * Metodo para mostrar los componentes u ocultar en pantalla cuando se va a editar con el primer objeto del ComboBox
     */
    private void elementsCB1(){
        principalView.lNombre.setVisible(true);
        principalView.tfName.setVisible(true);
        principalView.lAge.setVisible(true);
        principalView.tfAge.setVisible(true);
        principalView.lDescription.setVisible(true);
        principalView.taDescription.setVisible(true);

        principalView.lStats.setVisible(true);
        principalView.lFuerza.setVisible(true);
        principalView.tfFuerza.setVisible(true);
        principalView.lAgilidad.setVisible(true);
        principalView.tfAgilidad.setVisible(true);
        principalView.lDestreza.setVisible(true);
        principalView.tfDestreza.setVisible(true);
        principalView.lPercecion.setVisible(true);
        principalView.tfPercepcion.setVisible(true);
        principalView.lInteligencia.setVisible(true);
        principalView.tfInteligencia.setVisible(true);

        principalView.lProduce.setVisible(false);
        principalView.lDamage.setVisible(false);
        principalView.tfDamage.setVisible(false);
        principalView.lCuracion.setVisible(false);
        principalView.tfHealing.setVisible(false);
        principalView.lConsume.setVisible(false);
        principalView.lMunicion.setVisible(false);
        principalView.tfAmmoUse.setVisible(false);
        principalView.lEnergia.setVisible(false);
        principalView.tfEnergyUse.setVisible(false);
    }

    /**
     * Metodo para mostrar los componentes u ocultar en pantalla cuando se va a editar con el segundo objeto del
     * ComboBox
     */
    private void elementsCB2(){
        principalView.lNombre.setVisible(true);
        principalView.tfName.setVisible(true);
        principalView.lAge.setVisible(false);
        principalView.tfAge.setVisible(false);
        principalView.lDescription.setVisible(true);
        principalView.taDescription.setVisible(true);

        principalView.lStats.setVisible(true);
        principalView.lFuerza.setVisible(true);
        principalView.tfFuerza.setVisible(true);
        principalView.lAgilidad.setVisible(true);
        principalView.tfAgilidad.setVisible(true);
        principalView.lDestreza.setVisible(true);
        principalView.tfDestreza.setVisible(true);
        principalView.lPercecion.setVisible(true);
        principalView.tfPercepcion.setVisible(true);
        principalView.lInteligencia.setVisible(true);
        principalView.tfInteligencia.setVisible(true);

        principalView.lProduce.setVisible(false);
        principalView.lDamage.setVisible(false);
        principalView.tfDamage.setVisible(false);
        principalView.lCuracion.setVisible(false);
        principalView.tfHealing.setVisible(false);
        principalView.lConsume.setVisible(false);
        principalView.lMunicion.setVisible(false);
        principalView.tfAmmoUse.setVisible(false);
        principalView.lEnergia.setVisible(false);
        principalView.tfEnergyUse.setVisible(false);
    }

    /**
     * Metodo para mostrar los componentes u ocultar en pantalla cuando se va a editar con el tercer objeto del ComboBox
     */
    private void elementsCB3(){
        principalView.lNombre.setVisible(true);
        principalView.tfName.setVisible(true);
        principalView.lAge.setVisible(false);
        principalView.tfAge.setVisible(false);
        principalView.lDescription.setVisible(true);
        principalView.taDescription.setVisible(true);

        principalView.lStats.setVisible(true);
        principalView.lFuerza.setVisible(true);
        principalView.tfFuerza.setVisible(true);
        principalView.lAgilidad.setVisible(true);
        principalView.tfAgilidad.setVisible(true);
        principalView.lDestreza.setVisible(true);
        principalView.tfDestreza.setVisible(true);
        principalView.lPercecion.setVisible(true);
        principalView.tfPercepcion.setVisible(true);
        principalView.lInteligencia.setVisible(true);
        principalView.tfInteligencia.setVisible(true);

        principalView.lProduce.setVisible(true);
        principalView.lDamage.setVisible(true);
        principalView.tfDamage.setVisible(true);
        principalView.lCuracion.setVisible(true);
        principalView.tfHealing.setVisible(true);
        principalView.lConsume.setVisible(true);
        principalView.lMunicion.setVisible(true);
        principalView.tfAmmoUse.setVisible(true);
        principalView.lEnergia.setVisible(true);
        principalView.tfEnergyUse.setVisible(true);
    }

    /**
     * Metodo para mostrar u ocultar elementos en pantalla cuando se va a editar.
     */
    private void onlyCreate(){
        principalView.jp1.setVisible(true);
        principalView.jp2.setVisible(true);
        principalView.jp3.setVisible(true);
        principalView.cbObjetos.setVisible(true);
        principalView.cbClean.setVisible(true);

        if (!baseDeDatos.estadoConexion()) {
            principalView.jpJLedit.setVisible(true);
            principalView.jpTableDB.setVisible(false);
            buttonsEnabled();
        } else {
            principalView.jpJLedit.setVisible(false);
            principalView.jpTableDB.setVisible(true);
            buttonsEnabled();
        }

        principalView.jp3JL.setVisible(false);
        principalView.jp2TA.setVisible(false);
        principalView.jpBuscar.setVisible(false);
    }

    /**
     * Metodo para mostrar u ocultar elementos en pantalla cuando se va a listar.
     */
    private void onlyLists(){
        principalView.jp1.setVisible(false);
        principalView.jp2.setVisible(false);
        principalView.jp3.setVisible(false);
        principalView.cbObjetos.setVisible(false);
        principalView.cbClean.setVisible(false);

        principalView.jpJLedit.setVisible(false);
        principalView.jpTableDB.setVisible(false);

        principalView.jp3JL.setVisible(true);
        principalView.jp2TA.setVisible(true);
        principalView.jpBuscar.setVisible(true);

        principalView.tfBuscar.setText("");
        principalView.taData.setText("");
    }

    private void buttonsEnabled() {
        if (!baseDeDatos.estadoConexion()) {
            principalView.bSave.setEnabled(true);
            principalView.bRoute.setEnabled(true);
            principalView.bImport.setEnabled(true);
            principalView.bExport.setEnabled(true);
            principalView.bListar.setEnabled(true);
            principalView.cbAutoGuardar.setVisible(true);
            principalView.lGuardar.setVisible(true);
        } else {
            principalView.bSave.setEnabled(false);
            principalView.bRoute.setEnabled(false);
            principalView.bImport.setEnabled(false);
            principalView.bExport.setEnabled(false);
            principalView.bListar.setEnabled(false);
            principalView.cbAutoGuardar.setVisible(false);
            principalView.lGuardar.setVisible(false);
        }

    }

    /**
     * Metodo que limpia los tf, se usara cada vez que se crea un objeto, se cambia de tipo de objeto o cuando
     */
    private void cleantf(){
        principalView.tfName.setText("");
        principalView.tfAge.setText("");
        principalView.taDescription.setText("");
        principalView.tfAlcance.setText("");
        principalView.tfFuerza.setText("");
        principalView.tfAgilidad.setText("");
        principalView.tfDestreza.setText("");
        principalView.tfPercepcion.setText("");
        principalView.tfInteligencia.setText("");
        principalView.tfDamage.setText("");
        principalView.tfHealing.setText("");
        principalView.tfAmmoUse.setText("");
        principalView.tfEnergyUse.setText("");
        principalView.tfFindDB.setText("");
    }

    /**
     * Metodo que verifica el imput del programa para el primer tipo de objeto del Combobox
     * @return Boolean, que verifica si es correcto o no el controducido.
     */
    private boolean contentVerificate0(){
        if (principalView.tfName.getText().equals("") || Util.isIntegerParseInt(principalView.tfAge.getText().toString()) == null
                || Util.isFloatParseFloat(principalView.tfAlcance.getText().toString()) == null
                || principalView.taDescription.getText().toString().equals("") || Util.isIntegerParseInt(principalView.tfFuerza.getText().toString()) == null || Util.isIntegerParseInt(principalView.tfAgilidad.getText().toString()) == null || Util.isIntegerParseInt(principalView.tfDestreza.getText().toString()) == null || Util.isIntegerParseInt(principalView.tfPercepcion.getText().toString()) == null || Util.isIntegerParseInt(principalView.tfInteligencia.getText().toString()) == null) {
            return false;
        }
        else {
            return true;
        }
    }

    /**
     * Metodo que verifica el imput del programa para el segundo tipo de objeto del Combobox
     * @return Boolean, que verifica si es correcto o no el controducido.
     */
    private boolean contentVerificate1(){
        if (principalView.tfName.getText().equals("") || Util.isFloatParseFloat(principalView.tfAlcance.getText().toString()) == null
                || principalView.taDescription.getText().toString().equals("") || Util.isIntegerParseInt(principalView.tfFuerza.getText().toString()) == null || Util.isIntegerParseInt(principalView.tfAgilidad.getText().toString()) == null || Util.isIntegerParseInt(principalView.tfDestreza.getText().toString()) == null || Util.isIntegerParseInt(principalView.tfPercepcion.getText().toString()) == null || Util.isIntegerParseInt(principalView.tfInteligencia.getText().toString()) == null) {
            return false;
        }
        else {
            return true;
        }
    }

    /**
     * Metodo que verifica el imput del programa para el tercer tipo de objeto del Combobox
     * @return Boolean, que verifica si es correcto o no el controducido.
     */
    private boolean contentVerificate2(){
        if (principalView.tfName.getText().equals("") || Util.isFloatParseFloat(principalView.tfAlcance.getText().toString()) == null
                || principalView.taDescription.getText().toString().equals("") || Util.isIntegerParseInt(principalView.tfFuerza.getText().toString()) == null || Util.isIntegerParseInt(principalView.tfAgilidad.getText().toString()) == null || Util.isIntegerParseInt(principalView.tfDestreza.getText().toString()) == null || Util.isIntegerParseInt(principalView.tfPercepcion.getText().toString()) == null || Util.isIntegerParseInt(principalView.tfInteligencia.getText().toString()) == null ||
                Util.isIntegerParseInt(principalView.tfEnergyUse.getText().toString()) == null || Util.isIntegerParseInt(principalView.tfAmmoUse.getText().toString()) == null || Util.isIntegerParseInt(principalView.tfDamage.getText().toString()) == null || Util.isIntegerParseInt(principalView.tfHealing.getText().toString()) == null) {
            return false;
        }
        else {
            return true;
        }
    }

    /**
     * Metodo que establece el modelo del Jlist para cada el primer objeto del Combobox
     */
    private void listarCharacters(){
        lCharacter.clear();
        for (Character viewCharacter : model.getListCharacters()){
            lCharacter.addElement(viewCharacter);
        }
        principalView.lContent.setModel(lCharacter);
    }

    /**
     * Metodo que establece el modelo del Jlist para cada el segundo objeto del Combobox
     */
    private void listarObjects(){
        lObject.clear();
        for (Object viewObject : model.getListObjects()){
            lObject.addElement(viewObject);
        }
    }

    /**
     * Metodo que establece el modelo del Jlist para cada el tercer objeto del Combobox
     */
    private void listarSkills(){
        lSkill.clear();
        for (Skill viewSkill : model.getListSkills()){
            lSkill.addElement(viewSkill);
        }
    }

    /**
     * En este metodo se recogeran todos los procesos de los Jlist para los valores internos de los objetos seleccionados.
     * @param e - Recive el elemento que ha sido seleccionado en la interfaz.
     */
    @Override
    public void valueChanged(ListSelectionEvent e) {
        if(e.getValueIsAdjusting()){

            /*
             * En este apartado del metodo, se recogeran los valores del Jlist del objeto seleccionado dependiendo de cual sea este
             * y se devolveran a los Jtextfield para permitir modificarlos u obtener directamente el nombre del objeto
             * para borrarlo. Tambien puede cambiarse el nombre y dejar los demas parametros tal y como se han
             * recuperado para crear copias base de un elemento.
             */
            if (principalView.rbCreateEdit.isSelected()){
                if(principalView.cbObjetos.getSelectedIndex() == 0){
                    Character selectCharacter = (Character)principalView.lContent.getSelectedValue();
                    principalView.tfName.setText(selectCharacter.getName());
                    principalView.tfAge.setText(Integer.toString(selectCharacter.getAge()));
                    principalView.taDescription.setText(selectCharacter.getBiography());
                    principalView.tfAlcance.setText(Float.toString(selectCharacter.getAlcance()));
                    principalView.tfFuerza.setText(Integer.toString(selectCharacter.getStats()[0]));
                    principalView.tfAgilidad.setText(Integer.toString(selectCharacter.getStats()[1]));
                    principalView.tfDestreza.setText(Integer.toString(selectCharacter.getStats()[2]));
                    principalView.tfPercepcion.setText(Integer.toString(selectCharacter.getStats()[3]));
                    principalView.tfInteligencia.setText(Integer.toString(selectCharacter.getStats()[4]));
                }
                else if(principalView.cbObjetos.getSelectedIndex() == 1){
                    Object selectObject = (Object)principalView.lContent.getSelectedValue();
                    principalView.tfName.setText(selectObject.getName());
                    principalView.taDescription.setText(selectObject.getDescription());
                    principalView.tfAlcance.setText(Float.toString(selectObject.getAlcance()));
                    principalView.tfFuerza.setText(Integer.toString(selectObject.getStats()[0]));
                    principalView.tfAgilidad.setText(Integer.toString(selectObject.getStats()[1]));
                    principalView.tfDestreza.setText(Integer.toString(selectObject.getStats()[2]));
                    principalView.tfPercepcion.setText(Integer.toString(selectObject.getStats()[3]));
                    principalView.tfInteligencia.setText(Integer.toString(selectObject.getStats()[4]));
                }
                else if(principalView.cbObjetos.getSelectedIndex() == 2){
                    Skill selectSkill = (Skill)principalView.lContent.getSelectedValue();
                    principalView.tfName.setText(selectSkill.getName());
                    principalView.taDescription.setText(selectSkill.getDescription());
                    principalView.tfAlcance.setText(Float.toString(selectSkill.getAlcance()));
                    principalView.tfFuerza.setText(Integer.toString(selectSkill.getStats()[0]));
                    principalView.tfAgilidad.setText(Integer.toString(selectSkill.getStats()[1]));
                    principalView.tfDestreza.setText(Integer.toString(selectSkill.getStats()[2]));
                    principalView.tfPercepcion.setText(Integer.toString(selectSkill.getStats()[3]));
                    principalView.tfInteligencia.setText(Integer.toString(selectSkill.getStats()[4]));
                    principalView.tfAmmoUse.setText(Integer.toString(selectSkill.getAmmoUse()));
                    principalView.tfEnergyUse.setText(Integer.toString(selectSkill.getEnergyUse()));
                    principalView.tfHealing.setText(Integer.toString(selectSkill.getHealing()));
                    principalView.tfDamage.setText(Integer.toString(selectSkill.getDamage()));
                }
            }

            /*
             * En este apartado del metodo se obtiene que Jlist de los 3 disponibles en el modo de lista esta siendo seleccionado
             * para deseleccionar los demas Jlist y cargar el objeto seleccionado.
             */
            else if (principalView.rbList.isSelected()){
                if(e.getSource() == principalView.lCharacters){
                    Character selectCharacter = (Character)principalView.lCharacters.getSelectedValue();
                    principalView.taData.setText(selectCharacter.toStringComplete());
                    principalView.lObjects.clearSelection();
                    principalView.lSkills.clearSelection();
                }
                else if(e.getSource() == principalView.lObjects){
                    Object selectObject = (Object)principalView.lObjects.getSelectedValue();
                    principalView.taData.setText(selectObject.toStringComplete());
                    principalView.lCharacters.clearSelection();
                    principalView.lSkills.clearSelection();
                }
                else if(e.getSource() == principalView.lSkills){
                    Skill selectSkill = (Skill)principalView.lSkills.getSelectedValue();
                    principalView.taData.setText(selectSkill.toStringComplete());
                    principalView.lCharacters.clearSelection();
                    principalView.lObjects.clearSelection();
                }
            }
        }
    }

    /**
     * Metodo para buscar 2 parametros en los objetos guardados en los Jlist del objeto Character.
     * @param cadena - {@link String} Recive la cadena de terminos por la que buscara en los diferentes paramteros.
     */
    private void buscarCharacter(String cadena){
        lCharacter.clear();
        for(Character findCharacter : model.getListCharacters()){
            if(findCharacter.getName().startsWith(cadena)) {
                lCharacter.addElement(findCharacter);
            }
        }
        for(Character findCharacter : model.getListCharacters()){
            if(findCharacter.getBiography().startsWith(cadena)) {
                lCharacter.addElement(findCharacter);
            }
        }
    }

    /**
     * Metodo para buscar 2 parametros en los objetos guardados en los Jlist del objeto Object.
     * @param cadena - {@link String} Recive la cadena de terminos por la que buscara en los diferentes paramteros.
     */
    private void buscarObject(String cadena){
        lObject.clear();
        for(Object findObject : model.getListObjects()){
            if(findObject.getName().startsWith(cadena)) {
                lObject.addElement(findObject);
            }
        }
        for(Object findObject : model.getListObjects()){
            if(findObject.getDescription().startsWith(cadena)) {
                lObject.addElement(findObject);
            }
        }
    }

    /**
     * Metodo para buscar 2 parametros en los objetos guardados en los Jlist del objeto Skill.
     * @param cadena - {@link String} Recive la cadena de terminos por la que buscara en los diferentes paramteros.
     */
    private void buscarSkill(String cadena){
        lSkill.clear();
        for(Skill findSkill : model.getListSkills()){
            if(findSkill.getName().startsWith(cadena)) {
                lSkill.addElement(findSkill);
            }
        }
        for(Skill findSkill : model.getListSkills()){
            if(findSkill.getDescription().startsWith(cadena)) {
                lSkill.addElement(findSkill);
            }
        }
    }


    @Override
    public void windowOpened(WindowEvent e) {

    }

    /**
     * Metodo para que, cuando se cierre el programa, en caso de tener desactivado el guardado automatico, nos
     * pregunte si queremos guardar. Igualmente, una vez tengamos todo guardado o en caso de haber seleccionado
     * guardarlo, nos preguntara de nuevo o por primera vez si deseamos salir del programa.
     * @param e - Recive la accion de cerrar el programa.
     */
    @Override
    public void windowClosing(WindowEvent e) {
        int respuesta;
        if(modificate == true){
            respuesta = JOptionPane.showConfirmDialog(null,"Tiene cambios sin guardar. ¿Guardar antes de salir?","Guardar y salir", JOptionPane.YES_NO_OPTION);
            if(respuesta == JOptionPane.OK_OPTION){
                guardarFicheroConfiguracion();
                model.exportarXml(ficheroPrograma);
                modificate = false;
                principalView.lGuardar.setText("Cambios guardados");
                respuesta = JOptionPane.showConfirmDialog(null,"¿Desea cerrar el programa?","Guardar y salir", JOptionPane.YES_NO_OPTION);
                if(respuesta == JOptionPane.OK_OPTION){
                    System.exit(0);
                }
                else if(respuesta == JOptionPane.NO_OPTION) {
                    principalView.frame.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
                }
            }
            else if(respuesta == JOptionPane.NO_OPTION) {
                System.exit(0);
            }
        }
        else {
            respuesta = JOptionPane.showConfirmDialog(null,"¿Desea cerrar el programa?","Salir", JOptionPane.YES_NO_OPTION);
            if(respuesta == JOptionPane.OK_OPTION){
                guardarFicheroConfiguracion();

                if (!baseDeDatos.estadoConexion()) {
                    model.exportarXml(ficheroPrograma);
                }
                System.exit(0);
            }
            else if(respuesta == JOptionPane.NO_OPTION) {
                principalView.frame.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
            }
        }


    }

    @Override
    public void windowClosed(WindowEvent e) {
        //JOptionPane.showMessageDialog(null, "que te jodan");
    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }

    /**
     * Metodo que verifica el estado del Checkbox de autoguardado y devuelve el parametro correspondiente en los
     * botones de crear, modificar y borrar para conocer si los cambios han sido o no guardados y avisar a la hora
     * de cerrar el programa si existen cambios sin guardar.
     */
    private void autoguardado(){
        if(principalView.cbAutoGuardar.isSelected() == true){
            model.exportarXml(ficheroPrograma);
            modificate = false;
            principalView.lGuardar.setText("Cambios Guardados");
        }else{
            modificate = true;
            principalView.lGuardar.setText("Existen cambios por guardar");
        }
    }

    /**
     * Metodo para guardar un fichero de configuracion con los parametros de usuario, contraseña y ruta del XML
     */
    private void guardarFicheroConfiguracion(){
//        int userAdm = 0;
//        int userDefault = 0;
        Properties configuracion = new Properties();
        configuracion.setProperty("ruta", ruta.getAbsolutePath());
        configuracion.setProperty("fichero_programa", ficheroPrograma.getAbsolutePath());

//        configuracion.setProperty("userAdmin ", findUsuario.getUsuario());
//        configuracion.setProperty("passwordAdmin " + userAdm,findUsuario.getContrasena());
//        configuracion.setProperty("tipoUserAdmin " + userAdm, Integer.toString(findUsuario.getTipoUsuario()));
//        configuracion.setProperty("userDefault " + userDefault, findUsuario.getUsuario());
//        configuracion.setProperty("passwordDefault " + userDefault,findUsuario.getContrasena());
//        configuracion.setProperty("tipoUserDefault " + userDefault, Integer.toString(findUsuario.getTipoUsuario()));

//        for (Usuario findUsuario : model.getListUsuarios()) {
//            if(findUsuario.getTipoUsuario() == 0){
//                configuracion.setProperty("userAdmin" + userAdm, findUsuario.getUsuario());
//                configuracion.setProperty("passwordAdmin" + userAdm,findUsuario.getContrasena());
//                configuracion.setProperty("tipoUserAdmin" + userAdm, Integer.toString(findUsuario.getTipoUsuario()));
//                userAdm++;
//            } else{
//                configuracion.setProperty("userDefault" + userDefault, findUsuario.getUsuario());
//                configuracion.setProperty("passwordDefault" + userDefault,findUsuario.getContrasena());
//                configuracion.setProperty("tipoUserDefault" + userDefault, Integer.toString(findUsuario.getTipoUsuario()));
//                userDefault++;
//            }
//            }

        try {
            configuracion.store(new FileOutputStream("fichero.conf"),"fichero de configuracion");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Metodo para cargar un fichero de configuracion con los parametros de usuario, contraseña y ruta del XML
     */
    private void cargarFicheroConfiguracion(){
        Properties configuracion = new Properties();
        HashMap<String, String> properties = new HashMap<String, String>();
//        user = "";
//        password = "";
//        tipoUser = 2;
//        int indexID = 0;

//        try {
//            configuracion.load(new FileInputStream("fichero.conf"));
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//        for (String propertyName : configuracion.stringPropertyNames()) {
//            properties.put(propertyName, configuracion.getProperty(propertyName));
//        }
//
//        for (Map.Entry<String, String> getUsuario : properties.entrySet()) {
//            if (getUsuario.getKey().contains("user" + indexID)) {
//                user = getUsuario.getValue();
//            }
//            else if (getUsuario.getKey().contains("password" + indexID)) {
//                password = getUsuario.getValue();
//            }
//            else if (getUsuario.getKey().contains("tipoUser" + indexID)) {
//                tipoUser = Integer.parseInt(getUsuario.getValue());
//            }
//
//            if(!user.equals("") && !password.equals("") && tipoUser != 2){
//                nuevoUsuario(user, password, tipoUser);
//                user = "";
//                password = "";
//                tipoUser = 2;
//            }
//    }




        try {
            configuracion.load(new FileInputStream("fichero.conf"));
            ruta = new File(configuracion.getProperty("ruta"));
//            user = configuracion.getProperty("user");
//            password = configuracion.getProperty("password");
//            tipoUser = Integer.parseInt(configuracion.getProperty("tipoUser"));
//            nuevoUsuario(user, password, tipoUser);
            ficheroPrograma = new File(configuracion.getProperty("fichero_programa"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Metodo que permite al usuario elegir la ubicacion donde guardara la ruta del XML en caso de querer cambiar la
     * que viene por defecto.
     */
    private void userFichero(){
        jFileChooser = new JFileChooser();
        fileNameExtensionFilter = new FileNameExtensionFilter("xml", "XML");
        jFileChooser.setFileFilter(fileNameExtensionFilter);
        int ubicacionSelect = jFileChooser.showSaveDialog(null);
        if (ubicacionSelect == JFileChooser.APPROVE_OPTION){
            model.exportarXml(jFileChooser.getSelectedFile());
        }
    }

    private void especificarTabla(String consulta, boolean busqueda) {
        principalView.dtm.setRowCount(0);
        ResultSet resultSet = null;
        if (!busqueda) {
            principalView.cbFindDB.removeAllItems();
        }
        try {
            resultSet = baseDeDatos.consultaSeleccion(consulta);
            if (resultSet != null) {
                int numeroColumnas = resultSet.getMetaData().getColumnCount();
                index = new int[3];
                int k = 0;
                java.lang.Object[] filas = null;
                java.lang.Object[] columnas = new java.lang.Object[numeroColumnas];
                for (int i = 0; i < resultSet.getMetaData().getColumnCount(); i++) {
                    columnas[i] = resultSet.getMetaData().getColumnName(i + 1);
                    if (!busqueda) {
                        principalView.cbFindDB.addItem(columnas[i]);
                    }
                    if (resultSet.getMetaData().getColumnName(i + 1).equals("id") || resultSet.getMetaData().getColumnName(i + 1).equals("stats_all_id")) {
                        index[k] = i;
                        k++;
                    }
                }
                principalView.dtm.setColumnIdentifiers(columnas);

                while (resultSet.next()) {
                    filas = new java.lang.Object[numeroColumnas];
                    for (int i = 0; i < numeroColumnas; i++) {
                        filas[i] = resultSet.getObject(i + 1);
                    }
                    principalView.dtm.addRow(filas);
                }
                principalView.tDB.setModel(principalView.dtm);

                TableColumnModel tcm = principalView.tDB.getColumnModel();
//                tcm.removeColumn(tcm.getColumn(index[0]));
//                tcm.removeColumn(tcm.getColumn(index[1]));
//                tcm.removeColumn(tcm.getColumn(index[2]));

                for (int i = 2; i > -1; i--) {
                    tcm.removeColumn(tcm.getColumn(index[i]));
                }
            }
        } catch (SQLException s) {
            s.printStackTrace();
        }
    }

    @Override
    public void tableChanged(TableModelEvent e) {
        int filaInicial = e.getFirstRow();
        int filaFinal = e.getLastRow();
        int columnas = e.getColumn();
        switch (e.getType()) {
            case TableModelEvent.UPDATE:
                updateRow(e);
                break;
        }
    }

    private void tipeUser(int tipoUser) {
        if (tipoUser == 1) {
            principalView.bCreate.setEnabled(false);
            principalView.bModificate.setEnabled(false);
            principalView.bDelete.setEnabled(false);
        } else {
            principalView.bCreate.setEnabled(true);
            principalView.bDelete.setEnabled(true);
            if (baseDeDatos.estadoConexion()) {
                principalView.bModificate.setEnabled(false);
            } else {
                principalView.bModificate.setEnabled(true);
            }
        }
    }

    private void busquedaDB() {
        if (!principalView.tfFindDB.getText().equals("")) {
            if (principalView.cbFindDB.getSelectedIndex() < index[2]) {
                especificarTabla(baseDeDatos.queryObtenerBusqueda(principalView.cbObjetos.getSelectedIndex(), principalView.cbFindDB.getSelectedItem().toString(), principalView.cbFindValueDB.getSelectedItem().toString(), principalView.tfFindDB.getText().toString(), false, false), true);
            } else if (principalView.cbFindDB.getSelectedIndex() >= index[2]) {
                especificarTabla(baseDeDatos.queryObtenerBusqueda(principalView.cbObjetos.getSelectedIndex(), principalView.cbFindDB.getSelectedItem().toString(), principalView.cbFindValueDB.getSelectedItem().toString(), principalView.tfFindDB.getText().toString(), true, false), true);
            }
        } else {
            if (principalView.cbObjetos.getSelectedIndex() == 0) {
                System.out.println("DINEROS");
                especificarTabla(Constantes.queryListarCharactersAndStats, false);
            } else if (principalView.cbObjetos.getSelectedIndex() == 1) {
                especificarTabla(Constantes.queryListarObjectsAndStats, false);
            } else if (principalView.cbObjetos.getSelectedIndex() == 2) {
                especificarTabla(Constantes.queryListarSkillsAndStats, false);
            }
        }
    }

    public void updateRow(TableModelEvent e) {
        int filaInicial = e.getFirstRow();
        int filaFinal = e.getLastRow();
        int columnas = e.getColumn();
        if (filaInicial == TableModelEvent.HEADER_ROW) {
        } else {
            for (int i = filaInicial; i <= filaFinal; i++) {
                if (columnas != TableModelEvent.ALL_COLUMNS) {
                    String tabla = "";
                    String consulta = "";
                    int otherTable = index[1];

                    int selectColumn = principalView.tDB.getSelectedColumn();
                    String columna = principalView.dtm.getColumnName(e.getColumn());
                    String valor = (String) principalView.dtm.getValueAt(e.getLastRow(), e.getColumn());
                    int id = Integer.parseInt(principalView.dtm.getValueAt(e.getFirstRow(), 0).toString());
                    int index = id;

                    if (principalView.cbObjetos.getSelectedIndex() == 0) {
                        tabla = "personajes";
                        consulta = Constantes.queryListarCharactersAndStats;
                    } else if (principalView.cbObjetos.getSelectedIndex() == 1) {
                        tabla = "objetos";
                        consulta = Constantes.queryListarObjectsAndStats;
                    } else if (principalView.cbObjetos.getSelectedIndex() == 2) {
                        tabla = "habilidades";
                        consulta = Constantes.queryListarSkillsAndStats;
                    }

                    if (selectColumn >= (otherTable - 1)) {
                        try {
                            ResultSet test;
                            test = baseDeDatos.consultaSeleccion(baseDeDatos.queryObtenerStats(tabla, principalView.dtm.getValueAt(principalView.tDB.getSelectedRow(), 1).toString()));
                            while (test.next()) {
                                String str = test.getString("S.id");
                                index = Integer.parseInt(str);
                            }
                        } catch (SQLException e1) {
                            e1.printStackTrace();
                        }
                        tabla = "stats_all";
                    }
                    JOptionPane.showMessageDialog(null, tabla + "/" + columna + "/" + valor + "/" + index);
                    baseDeDatos.actualizarValorTabla(tabla, columna, valor, index);
                    especificarTabla(consulta, false);
                }
            }
        }
    }

    private void prepareDBXML() {
        ResultSet resultSet = null;

        model.setListsNew();
        lCharacter = new DefaultListModel<Character>();
        lObject = new DefaultListModel<Object>();
        lSkill = new DefaultListModel<Skill>();

        try {
            resultSet = baseDeDatos.consultaSeleccion("SELECT X.*, S.* FROM stats_all S, personajes X WHERE X.stats_all_id = S.id");
            if (resultSet != null) {
                int numeroColumnas = resultSet.getMetaData().getColumnCount();
                java.lang.Object[] filas = null;
                java.lang.Object[] columnas = new java.lang.Object[numeroColumnas];
                for (int i = 0; i < resultSet.getMetaData().getColumnCount(); i++) {
                    columnas[i] = resultSet.getMetaData().getColumnName(i + 1);
                }

                while (resultSet.next()) {
                    filas = new java.lang.Object[numeroColumnas];
                    for (int i = 0; i < numeroColumnas; i++) {
                        filas[i] = resultSet.getObject(i + 1);
                    }
                    ArrayList<String> textTest = new ArrayList<>();
                    for (int j = 0; j < numeroColumnas; j++) {
                        textTest.add(filas[j].toString());
                    }
                    model.newCharacter(textTest.get(1), Integer.parseInt(textTest.get(2)), Float.parseFloat(textTest.get(3)), textTest.get(4), Integer.parseInt(textTest.get(8)), Integer.parseInt(textTest.get(9)), Integer.parseInt(textTest.get(10)), Integer.parseInt(textTest.get(11)), Integer.parseInt(textTest.get(12)));
                    model.getListCharacters().get(model.getListCharacters().size() - 1).setCreationDate(Util.isDateParseDate(textTest.get(5)));
                }
            }
        } catch (SQLException s) {
            s.printStackTrace();
        }

        try {
            resultSet = baseDeDatos.consultaSeleccion("SELECT X.*, S.* FROM stats_all S, objetos X WHERE X.stats_all_id = S.id");
            if (resultSet != null) {
                int numeroColumnas = resultSet.getMetaData().getColumnCount();
                java.lang.Object[] filas = null;
                java.lang.Object[] columnas = new java.lang.Object[numeroColumnas];
                for (int i = 0; i < resultSet.getMetaData().getColumnCount(); i++) {
                    columnas[i] = resultSet.getMetaData().getColumnName(i + 1);
                }

                while (resultSet.next()) {
                    filas = new java.lang.Object[numeroColumnas];
                    for (int i = 0; i < numeroColumnas; i++) {
                        filas[i] = resultSet.getObject(i + 1);
                    }
                    ArrayList<String> textTest = new ArrayList<>();
                    for (int j = 0; j < numeroColumnas; j++) {
                        textTest.add(filas[j].toString());
                    }
                    model.newObject(textTest.get(1), textTest.get(3), Float.parseFloat(textTest.get(2)), Integer.parseInt(textTest.get(7)), Integer.parseInt(textTest.get(8)), Integer.parseInt(textTest.get(9)), Integer.parseInt(textTest.get(10)), Integer.parseInt(textTest.get(11)));
                    model.getListObjects().get(model.getListObjects().size() - 1).setCreationDate(Util.isDateParseDate(textTest.get(4)));
                }
            }
        } catch (SQLException s) {
            s.printStackTrace();
        }

        try {
            resultSet = baseDeDatos.consultaSeleccion("SELECT X.*, S.* FROM stats_all S, habilidades X WHERE X.stats_all_id = S.id");
            if (resultSet != null) {
                int numeroColumnas = resultSet.getMetaData().getColumnCount();
                java.lang.Object[] filas = null;
                java.lang.Object[] columnas = new java.lang.Object[numeroColumnas];
                for (int i = 0; i < resultSet.getMetaData().getColumnCount(); i++) {
                    columnas[i] = resultSet.getMetaData().getColumnName(i + 1);
                }

                while (resultSet.next()) {
                    filas = new java.lang.Object[numeroColumnas];
                    for (int i = 0; i < numeroColumnas; i++) {
                        filas[i] = resultSet.getObject(i + 1);
                    }
                    ArrayList<String> textTest = new ArrayList<>();
                    for (int j = 0; j < numeroColumnas; j++) {
                        textTest.add(filas[j].toString());
                    }
                    model.newSkill(textTest.get(1), textTest.get(3), Integer.parseInt(textTest.get(4)), Integer.parseInt(textTest.get(5)), Integer.parseInt(textTest.get(6)), Integer.parseInt(textTest.get(7)), Float.parseFloat(textTest.get(2)), Integer.parseInt(textTest.get(10)), Integer.parseInt(textTest.get(11)), Integer.parseInt(textTest.get(12)), Integer.parseInt(textTest.get(13)), Integer.parseInt(textTest.get(14)));
                    model.getListSkills().get(model.getListSkills().size() - 1).setCreationDate(Util.isDateParseDate(textTest.get(8)));
                }
            }
        } catch (SQLException s) {
            s.printStackTrace();
        }
    }

    private boolean verificacionNombreBaseDeDatos(String valor) {
        try {
            return baseDeDatos.consultaSeleccion(baseDeDatos.queryObtenerBusqueda(0, principalView.cbObjetos.getSelectedItem().toString(), "", valor, false, true)).next();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return true;
    }
}
