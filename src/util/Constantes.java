package util;

public class Constantes {
    public final static String queryListarCharactersAndStats = "SELECT P.*, S.* FROM personajes P, stats_all S WHERE P.stats_all_id = S.id";
    public final static String queryListarObjectsAndStats = "SELECT O.*, S.* FROM objetos O, stats_all S WHERE O.stats_all_id = S.id";
    public final static String queryListarSkillsAndStats = "SELECT H.*, S.* FROM Habilidades H, stats_all S WHERE H.stats_all_id = S.id";
}
