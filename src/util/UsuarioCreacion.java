package util;

import modelo.Model;

public class UsuarioCreacion {
    private Model model;

    public UsuarioCreacion(Model model) {
        this.model = model;
        model.nuevoUsuario("ra1db", "1234", 0);
        model.nuevoUsuario("invitado", "4321", 1);
        model.nuevoUsuario("root", "", 1);
        System.out.println(model.getListUsuarios().get(0).toString());
        System.out.println(model.getListUsuarios().get(1).toString());
    }
}
