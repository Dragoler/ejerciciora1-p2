package util;

import java.time.LocalDate;

public class Util {
    /**
     * Metodo util para convertir cadenas a numeros enteros.
     *
     * @param number {@link Integer}
     * @return
     */
    public static Integer isIntegerParseInt(String number) {
        try {
            return Integer.parseInt(number);
        } catch (NumberFormatException ex) {
        }
        return null;
    }

    /**
     * Metodo util para convertir cadenas a numeros con coma flotante
     *
     * @param number {@link Integer}
     * @return
     */
    public static Float isFloatParseFloat(String number) {
        try {
            return Float.parseFloat(number);
        } catch (NumberFormatException ex) {
        }
        return null;
    }

    public static LocalDate isDateParseDate(String fecha) {
        try {

            return LocalDate.parse(fecha);
        } catch (Exception e) {
            //e.printStackTrace();
            return null;
        }
    }
}
